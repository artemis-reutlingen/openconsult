from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_swagger_ui import get_swaggerui_blueprint
from flask_marshmallow import Marshmallow
from flask_restful import Api

from flask import Blueprint

app = Flask(
    __name__,
)
app.config.from_object(Config)

api_blueprint = Blueprint('api', __name__)  # use blueprint in order to add url prefix
api = Api(api_blueprint)

ma = Marshmallow(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

swagger_ui_blueprint = get_swaggerui_blueprint(
    Config.SWAGGER_URL,
    Config.API_URL,
    config={
        'app_name': 'TELEMed-Framework-Pseudonymization-Service'
    }
)
from app import urls

app.register_blueprint(swagger_ui_blueprint, url_prefix=Config.SWAGGER_URL)
app.register_blueprint(api_blueprint, url_prefix='/api/v1')  # adding url prefix here

# The bottom import is a workaround to circular imports, a common problem with Flask applications.
from app import models
