package de.telemed.healthimageuploader.ui.view

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import de.telemed.healthimageuploader.R
import de.telemed.healthimageuploader.ui.viewmodel.ScannerMode
import de.telemed.healthimageuploader.ui.viewmodel.ScannerViewModel
import kotlinx.android.synthetic.main.fragment_scanner.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

typealias AnalyzeListener = (value: String) -> Unit


class ScannerFragment : Fragment() {
    private val TAG = "QrCodeScannerFragment"
    private val requestPermissionCode = 42
    private lateinit var cameraExecutor: ExecutorService
    private val scanner = BarcodeScanning.getClient()
    private val model: ScannerViewModel by activityViewModels<ScannerViewModel>()
    private lateinit var imageAnalyzer: ImageAnalysis


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_scanner, container, false)
    }

    override fun onStart() {
        super.onStart()
        if(model.mode == ScannerMode.NOTHING) {
            Toast.makeText(context, getString(R.string.scanner_fragment_no_scanner_mode_text), Toast.LENGTH_LONG).show()
            return
        }
        if(ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)  == PackageManager.PERMISSION_GRANTED) {
            startCamera()
        }
        else {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA),requestPermissionCode )
        }
        cameraExecutor = Executors.newSingleThreadExecutor()
    }

    override fun onDestroy() {
        super.onDestroy()
        if(this::cameraExecutor.isInitialized && !cameraExecutor.isShutdown) {
            cameraExecutor.shutdown()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode) {
            requestPermissionCode -> {
                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startCamera()
                }
                else { Toast.makeText(activity, getString(R.string.scanner_fragment_camera_permission_denied), Toast.LENGTH_LONG).show()}
            }
        }
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener(Runnable
        {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(viewFinder.createSurfaceProvider())
                }

            imageAnalyzer = ImageAnalysis.Builder()
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()
                .also { it ->
                    it.setAnalyzer(cameraExecutor, QrCodeAnalyzer { value ->
                        imageAnalyzer.clearAnalyzer()
                        when(model.mode) {
                            ScannerMode.AUTH_TOKEN -> {
                                model.updateAuthToken(value)
                                findNavController().navigate(R.id.action_scannerFragment_to_tokenFragment)
                            }
                            ScannerMode.CASE_ID -> {
                                model.updateCaseId(value)
                                findNavController().navigate(R.id.action_scannerFragment_to_caseFragment)
                            }
                        }
                    })
                }

            // Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(
                    this, cameraSelector, preview, imageAnalyzer)

            } catch(exc: Exception) {
                Log.e(TAG, "Use case binding failed", exc)
            }
        }, ContextCompat.getMainExecutor(context))
    }

    private inner class QrCodeAnalyzer(private val listener: AnalyzeListener) : ImageAnalysis.Analyzer {
        @SuppressLint("UnsafeExperimentalUsageError")
        override fun analyze(imageProxy: ImageProxy) {
            val mediaImage = imageProxy.image
            mediaImage?.let {
                val image = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)
                BarcodeScanning.getClient().process(image)
                    .addOnSuccessListener { barcodes ->
                        for (barcode in barcodes) {
                            if(barcode.valueType == Barcode.TYPE_TEXT && barcode.rawValue != null) {
                                listener(barcode.rawValue!!)
                            }
                        }
                    }
                    .addOnFailureListener { Log.e(TAG, "$it") }
                    .addOnCompleteListener {
                        mediaImage.close()
                        imageProxy.close()
                    }
            }
        }
    }
}