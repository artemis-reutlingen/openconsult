from django.contrib import admin
from announcements.models import ServerAnnouncement


class ServerAnnouncementAdmin(admin.ModelAdmin):
    model = ServerAnnouncement
    list_display = (
        'created_at',
        'subject',
        'message',
        'is_active'
    )


admin.site.register(ServerAnnouncement, ServerAnnouncementAdmin)
