import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin as DjangoPermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.urls import reverse_lazy
from django.views.generic import DetailView, TemplateView, ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from guardian.mixins import PermissionRequiredMixin, PermissionListMixin
from guardian.shortcuts import assign_perm
from wkhtmltopdf.views import PDFTemplateResponse

from core.forms import BodystampForm, CaseForm, ReviewDiagnosisForm
from core.models import DermaConsultCase, DermaImage, Event, CaseQueueItem, CaseMessage, ReviewDiagnosis
from core.notifications import EMailNotifier

LOG = logging.getLogger(__name__)


class Dashboard(LoginRequiredMixin, TemplateView):
    template_name = 'core/dashboard.html'


class CaseList(PermissionListMixin, ListView):
    model = DermaConsultCase
    paginate_by = 10
    permission_required = 'core.access_dermaconsultcase'


class CreateCase(DjangoPermissionRequiredMixin, CreateView):
    model = DermaConsultCase
    form_class = CaseForm
    permission_required = 'core.add_dermaconsultcase'

    def form_valid(self, form):
        case_object = form.save(commit=False)
        case_object.requester = self.request.user
        case_object.save()

        # TODO: DUPLICATED CODE FROM API/VIEWS -> Create Case logic -> probably best inside model
        assign_perm('core.access_dermaconsultcase', self.request.user, case_object)
        assign_perm('core.edit_dermaconsultcase_anamnesis', self.request.user, case_object)
        assign_perm('core.publish_dermaconsultcase', self.request.user, case_object)
        assign_perm('core.remove_dermaconsultcase', self.request.user, case_object)

        return super(CreateCase, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.id})


class DeleteCase(PermissionRequiredMixin, DeleteView):
    model = DermaConsultCase
    permission_required = 'core.remove_dermaconsultcase'
    template_name = 'core/snippets/dermaconsultcase_confirm_delete.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.get_object().status == DermaConsultCase.CaseStatus.REGISTERED:
            raise PermissionDenied
        else:
            return super(DeleteCase, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('case-list')


class CloseCase(UpdateView):
    model = DermaConsultCase
    fields = []
    template_name = 'core/close_case_confirm.html'

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.id})

    def form_valid(self, form):
        Event.objects.create(case=self.object, description=Event.EventType.CASE_CLOSED)
        self.object.status = DermaConsultCase.CaseStatus.REVIEWED
        self.object.save()

        EMailNotifier.case_finalized(self.object)

        return super(CloseCase, self).form_valid(form)


class UpdateAnamnesis(PermissionRequiredMixin, UpdateView):
    model = DermaConsultCase
    form_class = CaseForm
    permission_required = 'core.edit_dermaconsultcase_anamnesis'
    return_403 = True

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.id})

    def form_valid(self, form):
        Event.objects.create(case=self.object, description=Event.EventType.ANAMNESIS_REVISED)
        return super(UpdateAnamnesis, self).form_valid(form)


class CaseDetail(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = DermaConsultCase
    fields = '__all__'
    permission_required = 'core.access_dermaconsultcase'
    return_403 = True

    def get_context_data(self, **kwargs):
        context = super(CaseDetail, self).get_context_data(**kwargs)

        from django.contrib.staticfiles import finders
        from core.utils import draw_coordinates_on_image_as_base64 as draw_coords

        bodymap_image_base = finders.find('core/bodymap_big.jpg')
        painted_bodymap = draw_coords(bodymap_image_base, 326, 400, self.object.anamnesis_bodystamp_coordinates)
        context['painted_bodymap_small'] = painted_bodymap

        return context


class AddImage(LoginRequiredMixin, DjangoPermissionRequiredMixin, CreateView):
    model = DermaImage
    fields = ['image', 'description']
    permission_required = 'core.add_dermaimage'

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.case_id})

    def form_valid(self, form):
        image = form.save(commit=False)
        case_id = self.kwargs.get('pk')
        case_object = DermaConsultCase.objects.get(pk=case_id)
        image.case = case_object

        image.save()
        user = self.request.user
        assign_perm('core.delete_image', user, image)

        return super(AddImage, self).form_valid(form)


class DeleteImage(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = DermaImage
    permission_required = 'core.delete_image'

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.case_id})


class EditImage(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = DermaImage
    permission_required = 'core.delete_image'
    fields = ['description']

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.case_id})


# Also serves as case publishing endpoint!
class CreateCaseQueueItem(LoginRequiredMixin, CreateView):
    model = CaseQueueItem
    fields = ['priority']

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.case_id})

    def form_valid(self, form):
        queue_item = form.save(commit=False)
        case_id = self.kwargs.get('pk')
        case_object = DermaConsultCase.objects.get(pk=case_id)

        if not self.request.user.has_perm('core.publish_dermaconsultcase', case_object):
            raise PermissionDenied
        elif not case_object.status == DermaConsultCase.CaseStatus.REGISTERED:
            raise PermissionDenied
        else:
            queue_item.case = case_object
            queue_item.target_region = self.request.user.assigned_region
            queue_item.save()
            case_object.status = case_object.CaseStatus.PUBLISHED
            case_object.save()
            Event.objects.create(case=case_object, description=Event.EventType.CASE_PUBLISHED)

            return super(CreateCaseQueueItem, self).form_valid(form)


class CreateMessage(LoginRequiredMixin, CreateView):
    model = CaseMessage
    fields = ['message']

    # permission_required = 'core.access_dermaconsultcase'

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.case_id})

    def form_valid(self, form):
        case_message = form.save(commit=False)
        case_id = self.kwargs.get('pk')
        case_object = DermaConsultCase.objects.get(pk=case_id)
        case_message.case = case_object
        case_message.author = self.request.user

        case_message.save()
        Event.objects.create(case=case_object, description=Event.EventType.NEW_MESSAGE)

        EMailNotifier.new_message_on_case(case_object, self.request.user)

        return super(CreateMessage, self).form_valid(form)


class EditFinding(PermissionRequiredMixin, UpdateView):
    model = DermaConsultCase
    fields = (
        'review_finding',
        'review_diagnosis',
        'review_expected_trend',
        'review_therapy_suggestion',
        'review_is_urgent',
        'review_dermatologist_visit_necessary',
    )
    permission_required = 'core.edit_dermaconsultcase_finding'
    return_403 = True

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.id})

    def form_valid(self, form):
        Event.objects.create(case=self.object, description=Event.EventType.ANAMNESIS_REVISED)
        return super(EditFinding, self).form_valid(form)


class EditBodystamp(PermissionRequiredMixin, UpdateView):
    model = DermaConsultCase
    permission_required = 'core.edit_dermaconsultcase_anamnesis'
    return_403 = True
    template_name = 'core/bodystamp_form.html'
    form_class = BodystampForm

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.id})

    def form_invalid(self, form):
        return super(EditBodystamp, self).form_invalid(form)

    def form_valid(self, form):
        return super(EditBodystamp, self).form_valid(form)


class PDFReport(PermissionRequiredMixin, DetailView):
    template = 'core/pdf_report.html'
    context = {}
    model = DermaConsultCase
    permission_required = 'core.access_dermaconsultcase'

    def get(self, request, *args, **kwargs):
        self.context['object'] = self.get_object()
        from django.contrib.staticfiles import finders
        from core.utils import draw_coordinates_on_image_as_base64 as draw_coords

        bodymap_image_base = finders.find('core/bodymap_big.jpg')

        painted_bodymap = draw_coords(bodymap_image_base, 326, 400, self.get_object().anamnesis_bodystamp_coordinates)

        self.context['painted_bodymap_small'] = painted_bodymap

        return PDFTemplateResponse(
            request=request,
            template=self.template,
            filename=f"OpenDermaConsult_Report_{self.get_object().id}.pdf",
            context=self.context,
            show_content_in_browser=True,
            cmd_options={
                'margin-top': 15,
                'margin-left': 15,
                'margin-right': 15,
                'margin-bottom': 15,

            }
        )


class CreateReviewDiagnosis(LoginRequiredMixin, CreateView):
    model = ReviewDiagnosis
    # fields = ['diagnosis']
    form_class = ReviewDiagnosisForm

    # permission_required = 'core.access_dermaconsultcase'

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.case_id})

    def form_valid(self, form):
        diagnosis = form.save(commit=False)
        case_id = self.kwargs.get('pk')
        case_object = DermaConsultCase.objects.get(pk=case_id)
        diagnosis.case = case_object

        diagnosis.save()
        user = self.request.user
        assign_perm('core.delete_review_diagnosis', user, diagnosis)

        return super(CreateReviewDiagnosis, self).form_valid(form)


class DeleteReviewDiagnosis(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = ReviewDiagnosis
    permission_required = 'core.delete_review_diagnosis'

    def get_success_url(self):
        return reverse_lazy('case-detail', kwargs={'pk': self.object.case_id})
