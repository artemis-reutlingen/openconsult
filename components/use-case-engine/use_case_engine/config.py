from dataclasses import dataclass
import toml
from use_case_engine.gui import get_user_credentials_from_gui
from use_case_engine.loggers import LOGGER

class GdtMapperConfig:
    JSON_SCHEMA = {
        "type": "object",
        "properties": {
            "patient_id": {"type": "string"},
            "patient_first_name": {"type": "string"},
            "patient_last_name": {"type": "string"},
            "patient_gender": {"type": "string"},
        },
        "required": ["patient_id", "patient_first_name", "patient_last_name", "patient_gender"]
    }


class GlobalConstants:
    CFG_DIR = 'config/'
    REMOTE_CFG_FILE = f"{CFG_DIR}remote_config.toml"
    ID_MAPPING_CFG_FILE = f"{CFG_DIR}id_mapping_service.toml"
    PSEUDONYMIZATION_CFG_FILE = f"{CFG_DIR}pseudonymization_service.toml"
    USE_CASE_ID = 'opendermaconsult'


@dataclass
class RemoteServiceConfig:
    base_url: str
    auth_token: str
    server_pub_key_file: str
    client_cert_file: str
    client_key_file: str

    JSON_SCHEMA_UPDATE_PATIENT = {
        "type": "object",
        "properties": {
            "anamnesis_patient_age": {"type": "integer"},
            "anamnesis_patient_sex": {"type": "string"},
        },
        "required": ["anamnesis_patient_age", "anamnesis_patient_sex"]
    }

    class Endpoints:
        API_BASE = '/api/v1/'
        HELLO_SERVER = f"{API_BASE}hello/"
        POST_CASE = f"{API_BASE}cases/"
        OBTAIN_AUTH_TOKEN = f"{API_BASE}auth-token/"


@dataclass
class IdMappingServiceConfig:
    base_url: str

    class Endpoints:
        API_BASE = '/api/v1/'
        STORE_ID_PAIR = f"{API_BASE}mappings"
        RETRIEVE_ID = f"{API_BASE}mappings"


@dataclass
class PseudonymizationServiceConfig:
    base_url: str

    JSON_SCHEMA_PSEUDONYMIZED = {
        "type": "object",
        "properties": {
            "age": {"type": "integer"},
            "gender": {"type": "string"},
        },
        "required": ["age", "gender"]
    }

    class Endpoints:
        API_BASE = '/api/v1/'
        STORE_PATIENT_INFO = f"{API_BASE}patients"
        RETRIEVE_PATIENT_INFO = f"{API_BASE}patients"


def validate_remote_config(remote_config):
    if not remote_config.auth_token:
        LOGGER.info(remote_config)
        LOGGER.info("auth token is empty, let user enter credentials to obtain token, store to config")
        username, password = get_user_credentials_from_gui("Retrieve Auth Token from server")
        # remote_config = obtain_token_dump_to_file(remote_config, username, password)
