package de.telemed.healthimageuploader.ui.view

import android.os.Bundle
import android.view.*
import android.view.MenuInflater
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import de.telemed.healthimageuploader.R
import de.telemed.healthimageuploader.ui.viewmodel.CaseViewModel
import de.telemed.healthimageuploader.ui.viewmodel.ScannerMode
import de.telemed.healthimageuploader.ui.viewmodel.ScannerViewModel
import kotlinx.android.synthetic.main.card_view_case.*
import kotlinx.android.synthetic.main.card_view_case.view.*
import kotlinx.android.synthetic.main.fragment_case.*

class CaseFragment : Fragment() {
    private val model: CaseViewModel by activityViewModels()
    private val scannerModel: ScannerViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_case, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        case_to_image_button.setOnClickListener {
            findNavController().navigate(R.id.action_caseFragment_to_imageManagerFragment)
        }
        case_to_scanner_Button.setOnClickListener {
            scannerModel.mode = ScannerMode.CASE_ID
            findNavController().navigate(R.id.action_caseFragment_to_scannerFragment)
        }
    }

    override fun onStart() {
        super.onStart()
        if(model.isCaseIdAvailable() != null) {
            noCaseTextView.visibility = View.INVISIBLE
            caseCardView.visibility = View.VISIBLE
            caseCardView.caseIdTextView.caseIdTextView.text = model.isCaseIdAvailable()
        }
        else {
            noCaseTextView.visibility = View.VISIBLE
            caseCardView.visibility = View.INVISIBLE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.case_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.action_menu_to_auth_token -> {
                findNavController().navigate(R.id.action_caseFragment_to_tokenFragment)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

}