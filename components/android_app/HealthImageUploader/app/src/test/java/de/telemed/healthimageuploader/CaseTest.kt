package de.telemed.healthimageuploader

import org.junit.Test
import org.junit.Assert.*
import de.telemed.healthimageuploader.model.Case


class CaseTest {

    @Test
    fun addDifferentIds() {
        Case().addCase("1")
        assertEquals(Case.id, "1")
        assertEquals(Case().caseId(), "1")

        Case().addCase("2")
        assertEquals(Case.id, "2")
        assertEquals(Case().caseId(), "2")

        Case().addCase("324235390563jfjdsadSD")
        assertEquals(Case.id, "324235390563jfjdsadSD")
        assertEquals(Case().caseId(), "324235390563jfjdsadSD")
        assertTrue(Case.id != "1")
    }

    @Test
    fun clearCase(){
        Case().clear()
        assertEquals(Case.id, null)

        Case().addCase("1")
        Case().clear()
        assertEquals(Case.id, null)
        assertEquals(Case().caseId(), null)
    }
}