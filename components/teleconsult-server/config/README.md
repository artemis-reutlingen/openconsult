# Generate Server certificate (self-signed)

- go to subfolder scripts:
	- run ./generate_CA.sh
	- copy key + certificate to ../ssl/
- start nginx via docker-compose; certificate files will be copied into nginx container (/etc/ssl/)

# Generate Client certificate

- go to subfolder scripts:
	- adapt parameters for different users in generate_client_key.sh
	- run ./generate_client_key.sh
- *.pfx file can be imported into your browser
