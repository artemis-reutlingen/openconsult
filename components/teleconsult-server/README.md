# How to use Django development server without Docker
## Prepare
- ```cd django-project```
- ```virtualenv -p python3.8 venv && source venv/bin/activate```
- ```pip install -r requirements.txt```
- ```python manage.py migrate```
- ```python manage.py initdefaultregion```
- ```python manage.py initicd10 icd10_catalog_dummy.txt```
- ```python manage.py runserver```
## Use Webapp
- visite site via browser (probably http://127.0.0.1:8000)
- register test users manually inside webapp
- for GP: go to settings -> security to create Auth token
- for Derma: go to settings -> availability to enable case assignment
## Case Assignment Routine
- ```python manage.py assigncases```
