from app import db
from datetime import datetime, date


class Patient(db.Model):
    patient_id = db.Column(db.String(128), primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.now)
    patient_birth_date = db.Column(db.Date)
    patient_first_name = db.Column(db.String(128))
    patient_last_name = db.Column(db.String(128))
    patient_gender = db.Column(db.String(128))

    def __init__(self, patient_id, patient_birth_date, patient_first_name, patient_last_name, patient_gender):
        self.patient_id = patient_id
        self.patient_birth_date = patient_birth_date
        self.patient_first_name = patient_first_name
        self.patient_last_name = patient_last_name
        self.patient_gender = patient_gender

    def get_age(self):
        today = date.today()
        born = self.patient_birth_date
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
