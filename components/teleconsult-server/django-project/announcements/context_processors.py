from announcements.models import ServerAnnouncement


def server_announcements(request):
    # LOGGER.debug("Server announcement context processor is active!")
    all_active_announcements = ServerAnnouncement.objects.all().filter(is_active=ServerAnnouncement.YesNoChoice.YES)
    return {'server_announcements': all_active_announcements}
