from flask_restful import Resource
from app.models import Patient
from app.serializers import patient_schema_out, patients_schema_out, patient_schema
from flask import request
from app import db
from marshmallow import ValidationError
import json


class PatientResourceList(Resource):
    def get(self):
        patients = Patient.query.all()
        response_body = patients_schema_out.dump(patients)
        return response_body, 200

    def post(self):
        json_dict = request.get_json(force=True)

        try:
            new_patient = patient_schema.load(json_dict, session=db.session)
            db.session.add(new_patient)
            db.session.commit()
            return new_patient.patient_id, 201

        except ValidationError as err:
            return err.messages, 400


class PatientResource(Resource):
    def get(self, patient_id):
        patient = Patient.query.filter_by(patient_id=patient_id).first()
        if patient is not None:
            pseudonymization_flag = request.args.get('pseudonymized')
            if pseudonymization_flag is None:
                return 'Missing boolean query parameter pseudonymized.', 400

            elif pseudonymization_flag == "true":
                # TODO: create "pseudonymized" serialization
                response_dict = {}
                response_dict['age'] = patient.get_age()
                response_dict['gender'] = patient.patient_gender
                return response_dict, 200 # ??? no need to json.dumps(dict) ???
            elif pseudonymization_flag == "false":
                response_body = patient_schema_out.dump(patient)
                return response_body, 200
            else:
                return 'Illegal query parameter, must be "true" or "false"', 400
        else:
            return "Resource not found", 404
