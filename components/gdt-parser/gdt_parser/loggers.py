import logging.config

# logging.config.fileConfig('logging.conf', disable_existing_loggers=True)

# remove chardet debugging logmessages
logging.getLogger('chardet.charsetprober').setLevel(logging.INFO)

# LOGGER = logging.getLogger('gdt-parser-logger')
LOGGER = logging.getLogger(__name__)