from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, TemplateView
from django.views.generic.edit import UpdateView
from guardian.shortcuts import assign_perm
from rest_framework.authtoken.models import Token
from users.forms import DermatologistSignUpForm, GeneralPracticionerSignUpForm
from users.models import User, Dermatologist, GeneralPracticioner


class LandingPageView(TemplateView):
    template_name = 'users/landing_page.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('dashboard'))
        return super(LandingPageView, self).get(request, *args, **kwargs)


class SignUpView(TemplateView):
    template_name = 'users/signup_lander.html'


class MemberPageView(TemplateView):
    template_name = 'users/member_page.html'


class DermatologistSignUpView(CreateView):
    model = User
    form_class = DermatologistSignUpForm
    template_name = 'users/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = _('Dermatologist')
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
        return redirect('dashboard')


class DermatologistAvailabilityView(UpdateView):
    model = Dermatologist
    fields = ['is_currently_available']
    template_name = __package__ + '/dermatologist_availability_form.html'
    success_url = reverse_lazy('member_page')

    def form_valid(self, form):
        self.object = form.save()
        if form.changed_data:
            from core.models.regions import RegionReviewer
            print(f"new value: {form.cleaned_data['is_currently_available']}")
            if form.cleaned_data['is_currently_available']:
                RegionReviewer.objects.create(user=self.object.user, region=self.object.user.assigned_region)
            else:
                reviewer_entry = self.object.user.regionreviewer
                reviewer_entry.delete()

        return HttpResponseRedirect(self.get_success_url())


class GeneralPracticionerPseudoURLView(UpdateView):
    model = GeneralPracticioner
    fields = ['pseudo_service_url']
    template_name = __package__ + '/gp_pseudo_url_form.html'
    success_url = reverse_lazy('member_page')


class UserUpdateView(UpdateView):
    model = User
    fields = [
        'medical_title',
        'first_name',
        'last_name',
        'lanr',
        'phone_number',
        'address_streetname',
        'address_house_number',
        'address_city',
        'address_zip_code',
        'assigned_region'
    ]
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('member_page')


class UserEmailUpdateView(UpdateView):
    model = User
    fields = [
        'email'
    ]
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('member_page')


class GeneralPracticionerSignUpView(CreateView):
    model = User
    form_class = GeneralPracticionerSignUpForm
    template_name = 'users/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = _('General Practicioner')
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        assign_perm('core.add_dermaconsultcase', user)
        assign_perm('core.add_dermaimage', user)
        login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
        return redirect('dashboard')


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, _('Your password has been changed.'))
            return redirect('change-password')
        else:
            messages.error(request, _('The following errors occurred.'))
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'users/change_password.html', {
        'form': form
    })


class TokenUpdate(UpdateView):
    model = User
    fields = []
    template_name = 'users/gp_authtoken.html'

    def post(self, *args, **kwargs):
        user = self.get_object()
        token = Token.objects.filter(user=user).first()
        # Renewal of existing Auth token
        if token:
            token.delete()
            Token.objects.create(user=self.request.user)

        # token does not exist yet, create initially
        else:
            Token.objects.create(user=self.request.user)

        return super(TokenUpdate, self).get(self.request, *args, **kwargs)
