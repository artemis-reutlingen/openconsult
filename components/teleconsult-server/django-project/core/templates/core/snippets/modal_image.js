function openModal(imageIndex) {
    let modal = document.getElementById("myModal");
    let modalImageContent = document.getElementById("modal_image_content");
    let modalCaption = document.getElementById("caption");

    let imageToDisplay = document.getElementById("modalImage_" + imageIndex);

    modal.style.display = "block";
    modalImageContent.src = imageToDisplay.src;
    modalCaption.innerHTML = imageToDisplay.alt;


}

// Get the <span>element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span>(x), close the modal
span.onclick = function () {
    document.getElementById("myModal").style.display = "none";
}