from __future__ import annotations

import warnings
from abc import ABC, abstractmethod
from typing import Optional

from gdt_json_mapper.constants import GDTFieldTokens, GDTFieldIDs
from gdt_json_mapper.loggers import LOGGER

GDT_VERSION_KEY = GDTFieldIDs.GDT_9218
GDT_SENTENCE_ID_KEY = GDTFieldIDs.GDT_8000
GDT_VERSION_TOKEN = GDTFieldTokens.GDT_9218
GDT_SENTENCE_ID_TOKEN = GDTFieldTokens.GDT_8000

REQUIRED_KEYWORD = 'required'
FIELD_NAME_KEYWORD = 'field_name'


class Handler(ABC):
    """
    The Handler interface declares a method for building the chain of handlers.
    It also declares a method for executing a request.
    """

    @abstractmethod
    def set_next(self, handler: Handler) -> Handler:
        pass

    @abstractmethod
    def handle(self, request) -> Optional[dict]:
        pass


class AbstractHandler(Handler):
    """
    The default chaining behavior can be implemented inside a base handler
    class.
    """

    @property
    @abstractmethod
    def gdt_version(self) -> str:
        return ''

    @property
    @abstractmethod
    def gdt_sentence_id(self) -> str:
        return ''

    _next_handler: Handler = None

    def set_next(self, handler: Handler) -> Handler:
        self._next_handler = handler
        # Returning a handler from here will let us link handlers in a
        # convenient way like this:
        # monkey.set_next(squirrel).set_next(dog)
        return handler

    @abstractmethod
    def handle(self, request: dict) -> dict:
        if self._next_handler:
            return self._next_handler.handle(request)

        return None  # hmmm what about this return statement?

    @abstractmethod
    def get_mapper_dict(self) -> dict:
        return {}


"""
All Concrete Handlers either handle a request or pass it to the next handler in
the chain.
"""


def get_gdt_info(gdt_dict) -> dict:
    """
    Helper method for extracting general information about the gdt message.

    :param gdt_dict: original input dict
    :return: dictionary containing info for gdt version and sentence id
    """
    if GDT_VERSION_KEY in gdt_dict:
        gdt_version = gdt_dict[GDT_VERSION_KEY]
    else:
        raise Exception("GDT version not specified in input file!")

    if GDT_SENTENCE_ID_KEY in gdt_dict:
        gdt_sentence_id = gdt_dict[GDT_SENTENCE_ID_KEY]
    else:
        raise Exception("GDT sentence id not specified in input file!")

    return {
        GDT_VERSION_TOKEN: gdt_version,
        GDT_SENTENCE_ID_TOKEN: gdt_sentence_id
    }


class GDT_210_6302_Handler(AbstractHandler):
    def gdt_sentence_id(self) -> str:
        return '6302'

    def gdt_version(self) -> str:
        return '02.10'

    def handle(self, request: dict) -> dict:
        gdt_info = get_gdt_info(request)
        LOGGER.debug(f"Trying Handler for {self.gdt_version()}/{self.gdt_sentence_id()}")

        if gdt_info[GDT_VERSION_TOKEN] == self.gdt_version() and gdt_info[
            GDT_SENTENCE_ID_TOKEN] == self.gdt_sentence_id():
            return self.get_mapper_dict()
        else:
            return super().handle(request)

    def get_mapper_dict(self) -> dict:
        field_mapper = {}
        field_mapper['0102'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_0102}
        field_mapper['0103'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_0103}
        field_mapper['0132'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_0132}
        field_mapper['3000'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3000}
        field_mapper['3100'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3100}
        field_mapper['3101'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3101}
        field_mapper['3102'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3102}
        field_mapper['3103'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3103}
        field_mapper['3104'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3104}
        field_mapper['3105'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3105}
        field_mapper['3106'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3106}
        field_mapper['3107'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3107}
        field_mapper['3108'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3108}
        field_mapper['3110'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3110}
        field_mapper['3622'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3622}
        field_mapper['3623'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3623}
        field_mapper['3628'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3628}
        field_mapper['8000'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8000}
        field_mapper['8100'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8100}
        field_mapper['8315'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8315}
        field_mapper['8316'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8316}
        field_mapper['8402'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8402}
        field_mapper['8410'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8410}
        field_mapper['9206'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_9206}
        field_mapper['9218'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_9218}
        return field_mapper


class GDT_210_6301_Handler(AbstractHandler):
    def gdt_sentence_id(self) -> str:
        return '6301'

    def gdt_version(self) -> str:
        return '02.10'

    def handle(self, request: dict) -> dict:
        gdt_info = get_gdt_info(request)
        LOGGER.debug(f"Trying Handler for {self.gdt_version()}/{self.gdt_sentence_id()}")

        if gdt_info[GDT_VERSION_TOKEN] == self.gdt_version() and gdt_info[
            GDT_SENTENCE_ID_TOKEN] == self.gdt_sentence_id():
            return self.get_mapper_dict()
        else:
            return super().handle(request)

    def get_mapper_dict(self):

        field_mapper = {}

        field_mapper['3000'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3000}
        field_mapper['3100'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3100}
        field_mapper['3101'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3101}
        field_mapper['3102'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3102}
        field_mapper['3103'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3103}
        field_mapper['3104'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3104}
        field_mapper['3105'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3105}
        field_mapper['3106'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3106}
        field_mapper['3107'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3107}
        field_mapper['3108'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3108}
        field_mapper['3110'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3110}
        field_mapper['3622'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3622}
        field_mapper['3623'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3623}
        field_mapper['3628'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3628}
        field_mapper['8000'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8000}
        field_mapper['8100'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8100}
        field_mapper['8315'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8315}
        field_mapper['8316'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8316}
        field_mapper['9206'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_9206}
        field_mapper['9218'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_9218}

        return field_mapper


class GDT_200_6301_Handler(AbstractHandler):
    def gdt_sentence_id(self) -> str:
        return '6301'

    def gdt_version(self) -> str:
        return '02.00'

    def handle(self, request: dict) -> dict:
        gdt_info = get_gdt_info(request)
        LOGGER.debug(f"Trying Handler for {self.gdt_version()}/{self.gdt_sentence_id()}")

        if gdt_info[GDT_VERSION_TOKEN] == self.gdt_version() and gdt_info[
            GDT_SENTENCE_ID_TOKEN] == self.gdt_sentence_id():
            return self.get_mapper_dict()
        else:
            return super().handle(request)

    def get_mapper_dict(self) -> dict:
        field_mapper = {}

        field_mapper['3000'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3000}
        field_mapper['3100'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3100}
        field_mapper['3101'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3101}
        field_mapper['3102'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3102}
        field_mapper['3103'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3103}
        field_mapper['3104'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3104}
        field_mapper['3105'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3105}
        field_mapper['3106'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3106}
        field_mapper['3107'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3107}
        field_mapper['3108'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3108}
        field_mapper['3110'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3110}
        field_mapper['3622'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3622}
        field_mapper['3623'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3623}
        field_mapper['3628'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3628}
        field_mapper['4104'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_4104}
        field_mapper['4111'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_4111}
        field_mapper['4121'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_4121}
        field_mapper['8000'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8000}
        field_mapper['8100'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8100}
        field_mapper['8315'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8315}
        field_mapper['8316'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8316}
        field_mapper['9206'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_9206}
        field_mapper['9218'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_9218}

        return field_mapper


class GDT_200_6302_Handler(AbstractHandler):
    def gdt_sentence_id(self) -> str:
        return '6302'

    def gdt_version(self) -> str:
        return '02.00'

    def handle(self, request: dict) -> dict:
        gdt_info = get_gdt_info(request)
        LOGGER.debug(f"Trying Handler for {self.gdt_version()}/{self.gdt_sentence_id()}")

        if gdt_info[GDT_VERSION_TOKEN] == self.gdt_version() and gdt_info[
            GDT_SENTENCE_ID_TOKEN] == self.gdt_sentence_id():
            return self.get_mapper_dict()
        else:
            return super().handle(request)

    def get_mapper_dict(self) -> dict:
        warnings.warn("Warning, GDT 2.0/6302 is not specified by QMS. Trying prototype mapper for GDT 2.0/6302.")

        field_mapper = {}

        field_mapper['0201'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_0201}
        field_mapper['0203'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_0203}
        field_mapper['0211'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_0211}
        field_mapper['0212'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_0212}
        field_mapper['3000'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3000}
        field_mapper['3100'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3100}
        field_mapper['3101'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3101}
        field_mapper['3102'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3102}
        field_mapper['3103'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3103}
        field_mapper['3104'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3104}
        field_mapper['3105'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3105}
        field_mapper['3106'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3106}
        field_mapper['3107'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3107}
        field_mapper['3108'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3108}
        field_mapper['3110'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3110}
        field_mapper['3622'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3622}
        field_mapper['3623'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3623}
        field_mapper['3628'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_3628}
        field_mapper['8000'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8000}
        field_mapper['8100'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8100}
        field_mapper['8315'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8315}
        field_mapper['8316'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8316}
        field_mapper['8402'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_8402}
        field_mapper['9206'] = {REQUIRED_KEYWORD: False, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_9206}
        field_mapper['9218'] = {REQUIRED_KEYWORD: True, FIELD_NAME_KEYWORD: GDTFieldTokens.GDT_9218}

        return field_mapper





class NotImplementedHandler(AbstractHandler):
    def gdt_sentence_id(self) -> str:
        return '6302'

    def gdt_version(self) -> str:
        return '02.00'

    def handle(self, request: dict) -> None:
        gdt_info = get_gdt_info(request)
        LOGGER.debug("Reached NotImplementedHandler, raising Exception.")
        raise NotImplementedError(
            f"No Handler implemented for {gdt_info[GDT_VERSION_TOKEN]}/{gdt_info[GDT_SENTENCE_ID_TOKEN]}"
        )

    def get_mapper_dict(self) -> dict:
        return {}


class GDTHandler():
    def handle_gdt_dict(self, input_dict):
        gdt_210_6301 = GDT_210_6301_Handler()
        gdt_210_6302 = GDT_210_6302_Handler()
        gdt_200_6301 = GDT_200_6301_Handler()
        gdt_200_6302 = GDT_200_6302_Handler()
        error_handler = NotImplementedHandler()

        gdt_200_6301. \
            set_next(gdt_200_6302). \
            set_next(gdt_210_6301). \
            set_next(gdt_210_6302). \
            set_next(error_handler)

        result_dict = gdt_200_6301.handle(input_dict)

        return result_dict
