from django.forms import ModelForm, HiddenInput, TextInput, ModelChoiceField
from core.models import DermaConsultCase, ReviewDiagnosis
from icd10_catalog.models import Icd10Item
from dal import autocomplete


class CaseForm(ModelForm):
    class Meta:
        model = DermaConsultCase
        fields = [
            'anamnesis_patient_age',
            'anamnesis_patient_sex',
            'anamnesis_patient_occupation',
            'anamnesis_finding',
            'anamnesis_localisation',
            'anamnesis_current_medication',
            'anamnesis_existing_diagnoses',
            'anamnesis_complaint_type',
            'anamnesis_complaint_duration',
            'anamnesis_specific_question',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['anamnesis_finding'].required = True
        self.fields['anamnesis_specific_question'].required = True
        self.fields['anamnesis_patient_age'].required = True
        self.fields['anamnesis_patient_sex'].required = True


class BodystampForm(ModelForm):
    class Meta:
        model = DermaConsultCase
        fields = ['anamnesis_bodystamp_coordinates']
        widgets = {'anamnesis_bodystamp_coordinates': HiddenInput()}


class ReviewDiagnosisForm(ModelForm):
    diagnosis = ModelChoiceField(
        queryset=Icd10Item.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='icd-10-lookup',
            attrs={
                # Set some placeholder
                'data-placeholder': 'Autocomplete ...',
                # Only trigger autocompletion after 3 characters have been typed
                'data-minimum-input-length': 3,
                'width': '100%',
            },
        )
    )

    class Meta:
        model = ReviewDiagnosis
        fields = ('diagnosis',)
