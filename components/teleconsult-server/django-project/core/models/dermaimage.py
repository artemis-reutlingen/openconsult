from django.db import models
from uuid import uuid4
from telederm_service.settings import UPLOAD_DIR_CASE_IMAGES
import base64, os, logging
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

LOGGER = logging.getLogger(__name__)


def generate_image_upload_path(instance, filename):
    """
    This method generates the correct path for uploaded files:
    E.g. <project_root>/media/images/<case-uuid>/<image-uuid>.<extension>
    """
    instance.file_extension = filename.split('.')[-1]
    upload_path = "{}/{}.{}".format(UPLOAD_DIR_CASE_IMAGES, instance.filename, instance.file_extension)
    print(f"Generating Image upload path {upload_path}")
    return upload_path


class DermaImage(models.Model):
    uploaded_at = models.DateTimeField(auto_now=True)
    case = models.ForeignKey('core.DermaConsultCase', on_delete=models.CASCADE)
    image = models.ImageField(verbose_name=_('Image'), upload_to=generate_image_upload_path, blank=False, null=False)
    description = models.CharField(verbose_name=_('Description'), blank=True, default="", max_length=256)
    filename = models.UUIDField(default=uuid4, editable=False, auto_created=True, unique=True)

    def as_base64(self):
        if not os.path.isfile(self.image.path):
            LOGGER.error("Could not locate referenced image file while trying to convert image to base64 format!")
            return None

        with open(self.image.path, 'rb') as image_file:
            encoded_string = base64.b64encode(image_file.read()).decode('utf-8)')
            return 'data:image/%s;base64,%s' % (format, encoded_string)

    class Meta:
        permissions = (
            ('delete_image', 'Delete Image'),
        )




@receiver(post_save, sender=DermaImage)
def add_image_added_event(sender, instance=None, created=None, **kwargs):
    if created:
        from .event import Event
        Event.objects.create(case=instance.case, description=Event.EventType.IMAGE_ADDED)


@receiver(post_delete, sender=DermaImage)
def add_image_deleted_event(sender, instance=None, created=None, **kwargs):
    from .event import Event
    Event.objects.create(case=instance.case, description=Event.EventType.IMAGE_DELETED)
