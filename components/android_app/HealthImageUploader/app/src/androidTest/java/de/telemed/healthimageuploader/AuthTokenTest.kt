package de.telemed.healthimageuploader

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import de.telemed.healthimageuploader.model.AppContext
import de.telemed.healthimageuploader.model.AuthToken
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Before
import org.junit.Assert.*
import java.util.*

@RunWith(AndroidJUnit4::class)
class AuthTokenTest {

    @Before
    fun setUp() {
        AppContext.appContext = InstrumentationRegistry.getInstrumentation().targetContext
    }

    @Test
    fun updateAuthToken() {
        val AUTH_TOKEN_1 = "Test1"
        val AUTH_TOKEN_2 = "Test2"
        val AUTH_TOKEN_3 = "Test3"
        val authToken = AuthToken()

        authToken.update(AUTH_TOKEN_1)
        assertEquals(authToken.token(), AUTH_TOKEN_1)

        authToken.update(AUTH_TOKEN_2)
        assertEquals(authToken.token(), AUTH_TOKEN_2)

        authToken.update(AUTH_TOKEN_3)
        assertEquals(authToken.token(), AUTH_TOKEN_3)
    }

    @Test
    fun checkCreationDate() {
        val AUTH_TOKEN = "Test"
        val authToken = AuthToken()

        val dateTimeInMiSec = Calendar.getInstance().timeInMillis
        authToken.update(AUTH_TOKEN)
        val dateTimeInMiSecToken = authToken.creationTime()?.timeInMillis

        assertTrue(dateTimeInMiSecToken?.minus(dateTimeInMiSec)!! < 0.00001)
    }

    @Test
    fun checkConsistence() {
        val AUTH_TOKEN_First = "Token1"
        val AUTH_TOKEN_Second = "Token2"

        val authTokenFirst = AuthToken()
        val authTokenSecond = AuthToken()

        authTokenFirst.update(AUTH_TOKEN_First)
        assertEquals(authTokenFirst.token(), AUTH_TOKEN_First)
        assertEquals(authTokenSecond.token(), AUTH_TOKEN_First)

        authTokenFirst.update(AUTH_TOKEN_Second)
        assertEquals(authTokenFirst.token(), AUTH_TOKEN_Second)
        assertEquals(authTokenSecond.token(), AUTH_TOKEN_Second)
    }
}