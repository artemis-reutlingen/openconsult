package de.telemed.healthimageuploader.model

class Case {

    fun addCase(newId: String) {
        id = newId
    }

    fun caseId(): String? {
        return id
    }

    fun clear() {
        id = null
    }

    companion object Data {
        var id: String? = null
            private set
    }

}