package de.telemed.healthimageuploader.model

import android.net.Uri
import android.util.Log
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import okio.Buffer
import okio.IOException
import java.io.File
import java.net.HttpURLConnection
import java.net.URL


enum class ResponseState {
    SUCCESSFUL, ERROR, ACCESS_DENIED
}

object ServerAccess {
    private val TAG = "ServerAccess"
    private val baseUrl = "http://ip:port/api/v1" //TODO: Add settings in a file


    //TODO: Switch to OkHttp client
    suspend fun isTokenValid(token: String): Pair<Boolean, ResponseState>  {
        val url = "$baseUrl/hello"
        Log.e(TAG, "Try to validate token: $url")

        val connection = URL(url).openConnection() as HttpURLConnection
        try {
            connection.connectTimeout = 6000
            connection.readTimeout = 6000
            connection.setRequestProperty("Authorization", "Token $token")
            connection.connect()
            Log.d(TAG, "Tried to request GET $url -> Status code: ${connection.responseCode}")
            return when(connection.responseCode) {
                200 -> Pair(true, ResponseState.SUCCESSFUL)
                401 -> Pair(false, ResponseState.SUCCESSFUL)
                else -> Pair(false, ResponseState.SUCCESSFUL)
            }
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
            return Pair(false, ResponseState.ERROR)
        }
        finally {
            connection.disconnect()
        }
    }


    suspend fun postImageToServer(uri: Uri): Pair<Boolean, ResponseState> {
        val url = "$baseUrl/cases/${Case.id}/images/"
        Log.d(TAG, "Try to post image to $url")

        //val logging = HttpLoggingInterceptor()
        //logging.apply { logging.level = HttpLoggingInterceptor.Level.BODY }

        try {
            val file = File(uri.path)
            val requestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(
                    "file",
                    file.name,
                    file.asRequestBody("image/jpg".toMediaTypeOrNull())
                )
                .build()

            val request = Request.Builder()
                .url(url)
                .addHeader("Authorization", "Token ${AuthToken().token()}")
                .addHeader("Content-Type", "multipart/form-data")
                .post(requestBody)
                .build()

            //Log.e(TAG, responseBodyToString(request))

            val client = OkHttpClient()
            client.newCall(request).execute().use { response ->
                return when (response.code) {
                    201 -> Pair(true, ResponseState.SUCCESSFUL)
                    400 -> Pair(false, ResponseState.ERROR)
                    403 -> {
                        Log.e(TAG, "Access denied")
                        return Pair(false, ResponseState.ACCESS_DENIED)
                    }
                    else -> Pair(false, ResponseState.ERROR)
                }
            }

        } catch (e: java.lang.Exception) {Log.e(TAG, e.toString())}
        return Pair(true, ResponseState.ERROR)
    }

    private fun responseBodyToString(request: Request): String? {
        return try {
            val copy = request.newBuilder().build()
            val buffer = Buffer()
            copy.body!!.writeTo(buffer)
            buffer.readUtf8()
        } catch (e: IOException) {
            "function bodyToString: Not working"
        }
    }
}

