from django.db import models


class Icd10Item(models.Model):
    code = models.CharField(max_length=64, blank=False)
    description = models.CharField(max_length=1024, blank=False)

    def __repr__(self):
        return f"{self.code}, {self.description}"

    def __str__(self):
        return f"{self.code}, {self.description}"
