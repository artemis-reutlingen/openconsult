#!/bin/bash

echo "Copying compose environment into /container.env ..."
declare -p | grep -Ev 'BASHOPTS|BASH_VERSINFO|EUID|PPID|SHELLOPTS|UID' > /container.env

echo "Starting cron service ..."
service cron start

echo "Collecting static files for Django ..."
python manage.py collectstatic --no-input

echo "Starting gunicorn ..."
gunicorn telederm_service.wsgi -b 0.0.0.0:8080

