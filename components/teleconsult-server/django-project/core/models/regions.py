from django.db import models


class Region(models.Model):
    code = models.CharField(max_length=512, unique=True)
    last_regionreviewer_id = models.PositiveBigIntegerField(default=0)

    def __repr__(self):
        return self.code

    def __str__(self):
        return self.code


class RegionReviewer(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField('users.User', on_delete=models.CASCADE, null=False)
    region = models.ForeignKey('core.Region', on_delete=models.CASCADE, null=False)
