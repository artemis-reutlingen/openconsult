import unittest
from gdt_json_mapper.mapper import GDTMapper
from gdt_json_mapper.constants import GDTFieldTokens as tokens


class ParserTest(unittest.TestCase):
    mapper = GDTMapper()
    relevant_keys = [
        tokens.GDT_3000,
        tokens.GDT_3102,
        tokens.GDT_3101,
        tokens.GDT_3103,
        tokens.GDT_3110
    ]

    test_dicts = [
        {'8000': '6302', '8100': '00296', '9206': '3', '9218': '02.00', '0203': 'OSTERTAG',
         '0211': 'Dr. med. Andreas Arztmann', '0201': '541901600', '0212': '604154501', '3000': '1',
         '3101': 'Mustermann',
         '3102': 'Peter', '3103': '01011901', '3106': '72202 Nagold', '3107': 'Marktstr.3', '3108': '1', '3110': '1',
         '3622': '180', '3623': '55', '8402': 'ALLG00'},
        {'8000': '6301', '8100': '00242', '9218': '02.00', '3000': '1', '3101': 'Mustermann', '3102': 'Manuel',
         '3103': '30091964', '3107': 'Am Anger 12', '3110': '1', '3106': '96114 Hirschaid', '3622': '0.00',
         '3623': '0.00',
         '4111': '8380007', '4104': '71601', '4121': '02', '9901': 'DOCCOMFORT'},
        {'8000': '6302', '8100': '00310', '9206': '3', '9218': '02.10', '0102': 'MEDISTAR Praxiscomputer GmbH',
         '0103': 'Geräteanbindung GA_GDT', '0132': 'x2.13', '3000': '1', '3101': 'Testfrau', '3102': 'Lilli',
         '3103': '10061975', '3105': 'E121222110', '3106': 'D-30625 Hannover', '3107': 'Karl-Wiechert-Allee 64',
         '3108': '1', '3110': '2', '8402': 'TEST01'}
    ]

    def test_mapper(self):
        mapper = GDTMapper()

        for test_dict in self.test_dicts:
            mapper.map_gdt_dict_to_json(test_dict, relevant_keys=self.relevant_keys)

        self.assertEqual(True, True)
