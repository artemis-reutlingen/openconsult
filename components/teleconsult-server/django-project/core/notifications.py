from mail_templated import send_mail
from telederm_service.settings import HOST_NOREPLY_MAIL_ADDRESS, BASE_URL, LOGGER


class EMailNotifier:
    @classmethod
    def case_in_review_for_gp(cls, case):
        if case.requester.email:
            send_mail(
                'core/email/case_in_review_gp.html',
                {
                    'base_url': BASE_URL,
                    'gp': case.requester,
                    'dermatologist': case.examiner,
                    'case': case,
                },
                HOST_NOREPLY_MAIL_ADDRESS,
                [case.requester.email]
            )
        else:
            LOGGER.critical(f"Sending email to user {case.requester} failed. No email address is set.")

    @classmethod
    def case_in_review_for_dermatologist(cls, case):
        if case.examiner.email:
            send_mail(
                'core/email/case_in_review_dermatologist.html',
                {
                    'base_url': BASE_URL,
                    'gp': case.requester,
                    'dermatologist': case.examiner,
                    'case': case,
                },
                HOST_NOREPLY_MAIL_ADDRESS,
                [case.examiner.email]
            )
        else:
            LOGGER.critical(f"Sending email to user {case.examiner} failed. No email address is set.")

    @classmethod
    def new_message_on_case(cls, case, message_author):
        if message_author == case.requester:
            email_recipient = case.examiner
            author = message_author
        else:
            email_recipient = case.requester
            author = case.examiner

        if email_recipient.email:
            send_mail(
                'core/email/new_message_on_case.html',
                {
                    'base_url': BASE_URL,
                    'recipient': email_recipient,
                    'author': author,
                    'case': case,
                },
                HOST_NOREPLY_MAIL_ADDRESS,
                [email_recipient.email]
            )
        else:
            LOGGER.critical(f"Sending email to user {email_recipient} failed. No email address is set.")

    @classmethod
    def case_finalized(cls, case):
        if case.requester.email:
            send_mail(
                'core/email/case_finalized.html',
                {
                    'base_url': BASE_URL,
                    'gp': case.requester,
                    'dermatologist': case.examiner,
                    'case': case,
                },
                HOST_NOREPLY_MAIL_ADDRESS,
                [case.requester.email]
            )
        else:
            LOGGER.critical(f"Sending email to user {case.examiner} failed. No email address is set.")