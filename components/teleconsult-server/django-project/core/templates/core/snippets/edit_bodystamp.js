var hiddenValue = document.getElementById("id_anamnesis_bodystamp_coordinates").value;
var initCoordinates = JSON.parse(hiddenValue);
var coordinates = initCoordinates;

for (index = 0; index < initCoordinates.length; index++) {
    initCoordinates[index][0];
    initCoordinates[index][1];
}

document.getElementById("bodystamp_canvas").style.cursor = "crosshair";

$("#bodystamp_canvas").click(function (e) {
    let tmp = getPosition(e);
    let x = Number.parseFloat(tmp[0]).toFixed(2);
    let y = Number.parseFloat(tmp[1]).toFixed(2);
    coordinates.push([x, y])

    drawCoordinates(x, y);

    let coordinatesInput = document.getElementById("id_anamnesis_bodystamp_coordinates");
    coordinatesInput.value = JSON.stringify(coordinates);
});

function flushCoordinates() {
    coordinates = [];
    clearCanvas();
    let coordinatesInput = document.getElementById("id_anamnesis_bodystamp_coordinates");
    coordinatesInput.value = "[]";
}

function clearCanvas() {
    let canvas = document.getElementById("bodystamp_canvas");
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function getPosition(event) {
    let canvas = document.getElementById("bodystamp_canvas");
    let rect = canvas.getBoundingClientRect();
    let x = event.clientX - rect.left; // x == the location of the click in the document - the location (relative to the left) of the canvas in the document
    let y = event.clientY - rect.top; // y == the location of the click in the document - the location (relative to the top) of the canvas in the document

    return [x, y];
}

function drawCoordinates(x, y) {
    let pointSize = 5; // Change according to the size of the point.
    let ctx = document.getElementById("bodystamp_canvas").getContext("2d");
    ctx.globalAlpha = 0.9;
    ctx.fillStyle = "#ff2626";
    ctx.beginPath(); //Start path
    ctx.arc(x, y, pointSize, 0, Math.PI * 2, true); // Draw a point using the arc function of the canvas with a point structure.
    ctx.fill(); // Close the path and fill.
}


// draws the initial values from hidden input field
for (let index = 0; index < initCoordinates.length; index++) {
    drawCoordinates(initCoordinates[index][0], initCoordinates[index][1]);
}

