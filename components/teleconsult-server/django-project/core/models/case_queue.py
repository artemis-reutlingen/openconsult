from django.db import models
from django.utils.translation import gettext_lazy as _


class CaseQueueItem(models.Model):
    class PriorityValue(models.TextChoices):
        LOW = 'Low', _('Low')
        MEDIUM = 'Medium', _('Medium')
        HIGH = 'High', _('High')

    class QueueStatus(models.TextChoices):
        WAITING = 'Waiting', _('Waiting')
        ASSIGNED = 'Assigned', _('Assigned')
        DONE = 'Done', _('Done')

    queued_at = models.DateTimeField(auto_now=True)
    assigned_at = models.DateTimeField(blank=True, null=True)

    case = models.ForeignKey('core.DermaConsultCase', on_delete=models.CASCADE, blank=True, null=True)

    target_region = models.ForeignKey('core.Region', on_delete=models.CASCADE, null=True)

    priority = models.CharField(
        max_length=32,
        choices=PriorityValue.choices,
        default=PriorityValue.MEDIUM,
    )

    status = models.CharField(
        max_length=32,
        choices=QueueStatus.choices,
        default=QueueStatus.WAITING,
    )

    def requester(self):
        return self.case.requester

    def examiner(self):
        return self.case.examiner

    def case_id(self):
        return self.case.id