package de.telemed.healthimageuploader

import androidx.test.ext.junit.runners.AndroidJUnit4
import de.telemed.healthimageuploader.model.ServerAccess
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.*

//For this test the TeleDerm Server must be online and you have to set the right AuthToken and BaseUrl!
//Api version v1
@RunWith(AndroidJUnit4::class)
class AuthTokenServerTest {
    val AUTH_TOKEN = "aa6610670250754bffea1c1048c1877306d9af70"

    @Test
    fun checkValidToken() = runBlocking<Unit> {
        val value = ServerAccess.isTokenValid(AUTH_TOKEN)
        assertTrue(value.first)
    }

    @Test
    fun checkInvalidToken() = runBlocking<Unit> {
        val value = ServerAccess.isTokenValid("123")
        assertFalse(value.first)
    }
}