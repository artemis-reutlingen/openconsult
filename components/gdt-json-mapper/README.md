# GDT-JSON-Mapper

This component is responsible for:
- Take Python dictionary OR JSON file with "raw" GDT information as input
- Take list of "use-case relevant keys" and confirm the input object contains these
- Take list of "Mapper definitions" for allowed GDT Versions/Message Types
    - Replace GDT Message IDs with tokens from Mapper definitions
    - If necessary replace/translate GDT Values (e.g. Birthdate, Gender) according to definitions
- Return JSON object for further framework-internal use

# TODO

- outsource concrete mappings to json files, instantiate handlers from json definitions
- make mapper return JSON objects
- constants: how to apply this concept when defining handler in json?
- constants: it may be necessary to differentiate between GDT versions when defining string literals
- map GDT values (gender, birthdate) to string literals, needs some thinking how to implement this