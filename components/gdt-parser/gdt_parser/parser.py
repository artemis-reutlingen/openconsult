import chardet
from gdt_parser.loggers import LOGGER

GDT_LINE_LENGTH_INDEX = 3
GDT_FIELD_ID_INDEX = 7


# TODO: maybe add an actual validator, checking if file conforms to GDT specs
class GDTParser:
    def parse(self, filename: str) -> dict:
        """
        :param filename: Input GDT file
        :return: Python dictionary representing GDT file input
        """
        LOGGER.debug(f"Trying to parse GDT file: {filename}")

        file_encoding = self.__get_character_encoding(filename)

        gdt_content = self.__get_gdt_content(filename, file_encoding)

        gdt_dict = self.__create_gdt_message_dict(gdt_content)

        LOGGER.debug(f"Parsing result as dictionary: {gdt_dict}")

        return gdt_dict

    @staticmethod
    def __get_character_encoding(file):
        with open(file, 'rb') as f:
            raw_data = f.read()
            result = chardet.detect(raw_data)
            character_encoding = result['encoding']
            LOGGER.debug(f"Detected file encoding as {character_encoding} with {result['confidence']} confidence.")
            return character_encoding

    def __get_gdt_content(self, file_input, file_encoding):
        with open(file_input, "r", encoding=file_encoding) as file:
            content = file.readlines()

            return_list = []

            for line in content:
                return_list.append(self.__tokenize_gdt_line(line))

            return return_list

    @staticmethod
    def __create_gdt_message_dict(gdt_content):
        gdt_message_dictionary = {}
        for line in gdt_content:
            gdt_message_dictionary[line['field_id']] = line['field_content']
        return gdt_message_dictionary

    @staticmethod
    def __tokenize_gdt_line(line):
        # remove trailing whitespace / newline
        line = line.strip()

        line_length = line[:GDT_LINE_LENGTH_INDEX]
        field_id = line[GDT_LINE_LENGTH_INDEX:GDT_FIELD_ID_INDEX]
        field_content = line[GDT_FIELD_ID_INDEX:]

        return {'line_length': line_length, 'field_id': field_id, 'field_content': field_content}
