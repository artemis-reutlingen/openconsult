from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    is_dermatologist = models.BooleanField(default=False)
    is_general_practicioner = models.BooleanField(default=False)

    medical_title = models.CharField(verbose_name=_('Medical title'), max_length=255, null=False, blank=True)
    lanr = models.CharField(verbose_name=_('Lifelong Doctor ID'), max_length=255, null=True, blank=True, default=None)
    phone_number = models.CharField(verbose_name=_('Phone number'), max_length=255, null=True, blank=True, default=None)
    receive_emails = models.BooleanField(null=False, blank=False, default=True)

    address_streetname = models.CharField(verbose_name=_('Street'), max_length=255, null=True, blank=True, default=None)
    address_house_number = models.CharField(verbose_name=_('House number'), max_length=255, null=True, blank=True,
                                            default=None)
    address_city = models.CharField(verbose_name=_('City'), max_length=255, null=True, blank=True, default=None)
    address_zip_code = models.CharField(verbose_name=_('ZIP code'), max_length=255, null=True, blank=True, default=None)

    assigned_region = models.ForeignKey('core.Region', verbose_name=_('Region'), on_delete=models.SET_NULL, null=True)


class Dermatologist(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    is_currently_available = models.BooleanField(verbose_name=_('Currently available'), default=False)


class GeneralPracticioner(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    pseudo_service_url = models.CharField(max_length=255, null=True, blank=True, default="")
