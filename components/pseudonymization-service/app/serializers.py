from app import ma
from app.models import Patient


class PatientSchemaOut(ma.SQLAlchemyAutoSchema):
    # separate class for serializing
    class Meta:
        model = Patient
        fields = ("patient_id", "patient_first_name", "patient_last_name", "patient_birth_date", "patient_gender", "created_at")


class PatientSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Patient
        load_instance = True
        fields = ("patient_id", "patient_first_name", "patient_last_name", "patient_birth_date", "patient_gender")


patient_schema = PatientSchema()
patients_schema = PatientSchema(many=True)

patient_schema_out = PatientSchemaOut()
patients_schema_out = PatientSchemaOut(many=True)
