package de.telemed.healthimageuploader.ui.viewmodel

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.telemed.healthimageuploader.model.ResponseState
import de.telemed.healthimageuploader.model.ServerAccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File


enum class ImageManagerViewModelState {
    NO_IMAGE, IMAGE_ADDED, IMAGE_SEND, IMAGE_SEND_ERROR, IMAGE_SENT
}

class ImageManagerViewModel: ViewModel() {
    private val state = MutableLiveData(ImageManagerViewModelState.NO_IMAGE)
    private var imageUri: Uri? = null
    var bitmap: Bitmap? = null
        private set
    var lastSendError: ResponseState? = null
        private set


    fun state(): LiveData<ImageManagerViewModelState> {
        return state
    }

    fun addImage(uri: Uri) {
        if(state.value == ImageManagerViewModelState.IMAGE_SEND) {
            return
        }
        if(imageUri != null) {
            deleteImage()
            state.postValue(ImageManagerViewModelState.NO_IMAGE)
        }
        val bitmapSource = BitmapFactory.decodeFile(uri.path)
        val matrix = Matrix()
        matrix.postRotate(90.0F)
        bitmap = Bitmap.createBitmap(bitmapSource, 0, 0, bitmapSource.getWidth(), bitmapSource.getHeight(), matrix, true)

        imageUri = uri
        state.postValue(ImageManagerViewModelState.IMAGE_ADDED)
    }

    fun sendImage() {
        if(state.value == ImageManagerViewModelState.IMAGE_ADDED ||  state.value == ImageManagerViewModelState.IMAGE_SEND_ERROR) {
            state.postValue(ImageManagerViewModelState.IMAGE_SEND)
            viewModelScope.launch(Dispatchers.IO) {
                val response = ServerAccess.postImageToServer(imageUri!!)
                if(response.first) {
                    state.postValue(ImageManagerViewModelState.IMAGE_SENT)
                }
                else {
                    lastSendError = response.second
                    state.postValue(ImageManagerViewModelState.IMAGE_SEND_ERROR)
                }
            }
        }
    }

    fun deleteCurrentImage() {
        if(state.value == ImageManagerViewModelState.IMAGE_SEND) return
        deleteImage()
        state.postValue(ImageManagerViewModelState.NO_IMAGE)
    }

    private fun deleteImage() {
        if(imageUri != null) {
            File(imageUri.toString()).delete()
            imageUri = null
            bitmap = null
        }
    }

    override fun onCleared() {
        super.onCleared()
        deleteImage()
    }
}