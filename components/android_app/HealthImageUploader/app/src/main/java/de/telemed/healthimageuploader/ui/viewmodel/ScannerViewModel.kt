package de.telemed.healthimageuploader.ui.viewmodel

import androidx.lifecycle.ViewModel
import de.telemed.healthimageuploader.model.AuthToken
import de.telemed.healthimageuploader.model.Case

enum class ScannerMode {
    NOTHING, AUTH_TOKEN, CASE_ID
}

class ScannerViewModel: ViewModel() {
    private val authToken = AuthToken()
    var mode = ScannerMode.NOTHING

    fun updateAuthToken(tokenText: String) {
        authToken.update(tokenText)
    }

    fun updateCaseId(patientId: String) {
        Case().addCase(patientId)
    }
}