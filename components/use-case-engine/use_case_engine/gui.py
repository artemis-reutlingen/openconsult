import PySimpleGUI as sg


def get_user_credentials_from_gui(window_title: str):
    sg.theme('DefaultNoMoreNagging')

    layout = [[sg.Text('Provide your login credentials to obtain your API auth token.')],
              [sg.Text('Username'), sg.Input(key='-username-')],
              [sg.Text('Password'), sg.Input(key='-password-', password_char='*')],

              [sg.Button('Submit'), sg.Button('Cancel')]]

    # Create the Window
    window = sg.Window(window_title, layout)

    # Event Loop to process "events" and get the "values" of the inputs
    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Cancel':  # if user closes window or clicks cancel
            window.close()
            return None
        elif event == 'Submit':
            window.close()
            return values['-username-'], values['-password-']
