from django.core.management.base import BaseCommand, CommandError
from core.models.case_queue import CaseQueueItem
from telederm_service.settings import LOGGER


class Command(BaseCommand):
    help = 'Runs the case assignment routine.'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):

        queue_items = CaseQueueItem.objects.order_by('queued_at').filter(status=CaseQueueItem.QueueStatus.WAITING)
        from users.models import Dermatologist
        from core.models import RegionReviewer, Region
        available_dermatologists = Dermatologist.objects.filter(is_currently_available=True)
        LOGGER.info("*** START Case Assignment Routine ***")
        LOGGER.info("Checking Conditions for case assignment ...")
        if queue_items.exists():
            LOGGER.info(f"... {queue_items.count()} CaseQueueItems waiting for assignment!")

            if available_dermatologists.exists():
                LOGGER.info(f"... {available_dermatologists.count()}"
                            f" RegionReviewers available in total!")
                LOGGER.info(f"\nProceeding by iterating QueueItems and trying to "
                            f"find corresponding Reviewers for the corresponding region.")

                for queue_item in queue_items:
                    case_object = queue_item.case
                    region = queue_item.target_region
                    LOGGER.info(f"--> Processing QueueItem {queue_item.id} for case "
                                f"object {queue_item.case.id} (Region = {region}) ...")

                    # get list of available reviewers for this region
                    available_reviewers_for_region = RegionReviewer.objects.filter(region=region).order_by('id')

                    # simple case: there is only one reviewer available
                    if available_reviewers_for_region.count() == 1:
                        selected_regionreviewer = available_reviewers_for_region.first()
                        LOGGER.info("Assignment is trivial: 1 reviewer available")
                        LOGGER.info(f"QueueItem {queue_item.id} / Case {queue_item.case.id}"
                                    f" assigned to {selected_regionreviewer.user.username}")

                        case_object.assign_to_reviewer(selected_regionreviewer.user)
                        queue_item.status = queue_item.QueueStatus.ASSIGNED
                        queue_item.save()
                        region.last_regionreviewer_id = selected_regionreviewer.id
                        region.save()

                    # more than one reviewer available
                    else:
                        LOGGER.info("More than 1 reviewer available in this region, "
                                    "checking if new assignment cycle is necessary ...")
                        last_reviewer_in_list = available_reviewers_for_region.reverse()[0]

                        # if last reviewer in list of available ones equals region.last_reviewer
                        # -- > start cycle form start - select first from list
                        if last_reviewer_in_list.id == region.last_regionreviewer_id:
                            selected_regionreviewer = available_reviewers_for_region.first()
                            LOGGER.info("... yes! Selecting first Reviewer in list.")
                            case_object.assign_to_reviewer(selected_regionreviewer.user)
                            queue_item.status = queue_item.QueueStatus.ASSIGNED
                            queue_item.save()
                            region.last_regionreviewer_id = selected_regionreviewer.id
                            region.save()
                            LOGGER.info(f"QueueItem {queue_item.id} / Case {queue_item.case.id}"
                                        f" assigned to {selected_regionreviewer.user.username}\n")
                        # select the reviewer coming after region.last_reviewer from list
                        else:
                            filtered = available_reviewers_for_region.filter(
                                id__gt=region.last_regionreviewer_id)

                            selected_regionreviewer = available_reviewers_for_region.filter(
                                id__gt=region.last_regionreviewer_id).first()

                            LOGGER.info("... nope! Selecting next reviewer in order.")

                            case_object.assign_to_reviewer(selected_regionreviewer.user)
                            queue_item.status = queue_item.QueueStatus.ASSIGNED
                            queue_item.save()
                            region.last_regionreviewer_id = selected_regionreviewer.id
                            region.save()
                            LOGGER.info(f"QueueItem {queue_item.id} / Case {queue_item.case.id}"
                                        f" assigned to {selected_regionreviewer.user.username}\n")
            else:
                LOGGER.warn("No reviewers currently available.")
                LOGGER.warn("Conditions for case assignment not satisfied."
                            " This assignment cycle didn't do anything.")
        else:
            LOGGER.info("No CaseQueueItems currently waiting for assignment.")
            LOGGER.info("Conditions for case assignment not satisfied."
                        " This assignment cycle didn't do anything.")

        LOGGER.info("*** END Case Assignment Routine ***")
