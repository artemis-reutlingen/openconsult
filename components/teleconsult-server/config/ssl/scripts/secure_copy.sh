#!/bin/bash

# Skript zum kopieren eines Ordners vom
# Quellrechner zum Zielrechner

# Quellrechner
PFAD=/home/sven/TeleDerm2.0/telederm-2.0/server/src/

# Zielrechner Staging-Server
HOSTNAME=metiserv154.reutlingen-university.de
ZIELVERZEICHNIS=/home/artemis/update
USER=artemis

# Zielrechner Production-Server
#HOSTNAME=telederm-reutlingen.de
#ZIELVERZEICHNIS=/home/doerflis/update
#USER=doerflis

# scp -r Quelle Ziel
scp -r ${PFAD} $USER@${HOSTNAME}:${ZIELVERZEICHNIS}
