#!/bin/bash

# Create CA
openssl genrsa -aes256 -passout pass:GEHEIM -out ca.pass.key 4096
openssl rsa -passin pass:GEHEIM -in ca.pass.key -out server.key.pem
rm ca.pass.key

# Create CA root certificate:
openssl req -new -addext "subjectAltName = DNS:localhost" \-x509 -days 3650 -key server.key.pem -out server.cert.pem

# Copy files into config/ssl/ca-certs
cp server.key.pem ../ca-certs/server.key.pem
cp server.cert.pem ../ca-certs/server.cert.pem
