import os
import unittest
from app import app, db

TEST_DB = 'test.db'
BASE_DIR = app.root_path


class BasicTests(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(BASE_DIR, TEST_DB)
        self.app = app.test_client()
        db.drop_all()
        db.create_all()

        self.assertEqual(app.debug, False)

    def tearDown(self):
        db.drop_all()

    def test_post_id_mapping(self):
        request_body = {
            "internal_id": "id_interal_001234",
            "external_id": "id_external_0013232",
            "use_case_id": "id_usecase_0011231"
        }

        response = self.app.post('/api/v1/mappings', json=request_body, follow_redirects=False)
        response_content = response.get_data(as_text=True).strip()  # remove trailing newline

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response_content, "1", msg="Should return resource id 1")
