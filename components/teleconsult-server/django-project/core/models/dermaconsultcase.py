from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from multiselectfield import MultiSelectField
from guardian.shortcuts import assign_perm
from telederm_service.settings import LOGGER
from uuid import uuid4
from core.notifications import EMailNotifier


class DermaConsultCase(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False, auto_created=True)

    class CaseStatus(models.TextChoices):
        REGISTERED = 'Registered', _('Registered')
        PUBLISHED = 'Published', _('Published')
        IN_REVIEW = 'In Review', _('In Review')
        REVIEWED = 'Reviewed', _('Reviewed')

    class SexValue(models.TextChoices):
        MALE = 'M', _('Male')
        FEMALE = 'F', _('Female')

    class DermaComplaint(models.TextChoices):
        ITCHES = 'itches', _('itches')
        BURNS = 'burns', _('burns')
        WEEPS = 'weeps', _('weeps')
        HURTS = 'hurts', _('hurts')
        BLEEDS = 'bleeds', _('bleeds')

    class ComplaintDuration(models.TextChoices):
        DAYS = 'days', _('days')
        WEEKS = 'weeks', _('weeks')
        MONTHS = 'months', _('months')
        YEARS = 'years', _('years')

    class YesNoChoice(models.TextChoices):
        YES = 'yes', _('Yes')
        NO = 'no', _('No')

    requester = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='requesting_gp')
    examiner = models.ForeignKey('users.User', on_delete=models.CASCADE, null=True, blank=True,
                                 related_name='assigned_dermatologist')
    status = models.CharField(verbose_name=_("Case Status"), max_length=16, choices=CaseStatus.choices,
                              default=CaseStatus.REGISTERED,
                              blank=False, null=False)

    created_at = models.DateTimeField(auto_now_add=True)

    anamnesis_patient_age = models.IntegerField(verbose_name=_("Patient's age"), blank=True, null=False, default=18)
    anamnesis_patient_sex = models.CharField(verbose_name=_("Patient's sex"), max_length=16, choices=SexValue.choices,
                                             default=SexValue.MALE,
                                             blank=True, null=False)
    anamnesis_patient_occupation = models.CharField(verbose_name=_("Patient's occupation"), blank=True, max_length=256)
    anamnesis_finding = models.TextField(verbose_name=_("GP's finding"), blank=True)
    anamnesis_localisation = models.TextField(verbose_name=_("Localisation"), blank=True)
    anamnesis_current_medication = models.TextField(verbose_name=_("Current medication"), blank=True)
    anamnesis_existing_diagnoses = models.TextField(verbose_name=_("Existing diagnoses"), blank=True)
    anamnesis_complaint_type = MultiSelectField(verbose_name=_("Type of complaint"), choices=DermaComplaint.choices,
                                                blank=True)
    anamnesis_complaint_duration = models.CharField(verbose_name=_("Duration of complaint"), max_length=32,
                                                    choices=ComplaintDuration.choices, blank=True)
    anamnesis_specific_question = models.TextField(verbose_name=_("Specific question"), blank=True)

    anamnesis_bodystamp_coordinates = models.JSONField(verbose_name=_("Bodystamp Coordinates"), default=list,
                                                       blank=True)

    review_finding = models.TextField(verbose_name=_("Dermatologist's finding"), default="", blank=False)
    review_diagnosis = models.TextField(verbose_name=_("Diagnosis"), default="", blank=False)
    review_expected_trend = models.TextField(verbose_name=_("Expected trend"), default="", blank=True)
    review_therapy_suggestion = models.TextField(verbose_name=_("Suggested therapy"), default="", blank=True)

    review_is_urgent = models.CharField(max_length=64, verbose_name=_('Case is urgent'), choices=YesNoChoice.choices,
                                        default=YesNoChoice.NO,
                                        blank=False)
    review_dermatologist_visit_necessary = models.CharField(max_length=64,
                                                            verbose_name=_('Visit at dermatologist necessary'),
                                                            choices=YesNoChoice.choices, default=YesNoChoice.NO,
                                                            blank=False)

    class Meta:
        permissions = (
            ('access_dermaconsultcase', 'Can access dermaconsultcase'),
            ('edit_dermaconsultcase_anamnesis', 'Can edit anamnesis of dermaconsultcase'),
            ('edit_dermaconsultcase_finding', 'Can edit finding of dermaconsultcase'),
            ('publish_dermaconsultcase', 'Can publish case'),
            ('close_dermaconsultcase', 'Can close case'),
            ('remove_dermaconsultcase', 'Can remove case')

        )
        ordering = ['-created_at']

    def get_events(self):
        from .event import Event
        return Event.objects.filter(case_id=self.id)

    def get_last_event(self):
        from .event import Event
        return Event.objects.filter(case_id=self.id).last()

    def get_images(self):
        from .dermaimage import DermaImage
        return DermaImage.objects.filter(case_id=self.id).order_by('-uploaded_at')

    def get_messages(self):
        from .case_messages import CaseMessage
        return CaseMessage.objects.filter(case_id=self.id).order_by('-timestamp')

    def get_review_diagnoses(self):
        from .diagnosis import ReviewDiagnosis
        return ReviewDiagnosis.objects.filter(case_id=self.id)

    def assign_to_reviewer(self, user):
        self.examiner = user
        assign_perm('core.access_dermaconsultcase', user, self)
        assign_perm('core.edit_dermaconsultcase_finding', user, self)
        assign_perm('core.close_dermaconsultcase', user, self)

        from .event import Event
        Event.objects.create(case=self, description=Event.EventType.REVIEWER_ASSIGNED)
        self.status = self.CaseStatus.IN_REVIEW
        self.save()

        # TODO: probably all emails should be triggered from model
        EMailNotifier.case_in_review_for_gp(self)
        EMailNotifier.case_in_review_for_dermatologist(self)

    def update_patient_data(self, patient_age: int, patient_sex: str):
        self.anamnesis_patient_age = patient_age
        self.anamnesis_patient_sex = patient_sex
        from .event import Event
        Event.objects.create(case=self, description=Event.EventType.PATIENT_DATA_UPDATED)
        self.save()


@receiver(post_save, sender=DermaConsultCase)
def add_created_event(sender, instance=None, created=None, **kwargs):
    if created:
        from .event import Event
        Event.objects.create(case=instance)
