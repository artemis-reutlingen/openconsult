from django.core.management.base import BaseCommand
from core.models.regions import Region


class Command(BaseCommand):
    help = 'Creates the DEFAULT_REGION.'

    def handle(self, *args, **options):
        self.stdout.write("*** Creating DEFAULT_REGION object in database ***")

        default_region = Region.objects.create(code="DEFAULT_REGION")
        default_region.save()

        self.stdout.write(self.style.SUCCESS(f"Done."))
