import base64
import json
import logging
import os
import io

from PIL import Image, ImageDraw

LOGGER = logging.getLogger(__name__)


def draw_coordinates_on_image_as_base64(image_file, width, height, bodystamp_coordinates, format='png'):
    """
    :param `image_file` for the complete path of image.
    :param `format` is format for image, eg: `png` or `jpg`.
    """
    # LOGGER.debug("Drawing bodystamp points on bodystamp image.")

    if not os.path.isfile(image_file):
        LOGGER.error("Could not locate the referenced image file for bodystamp pic!")
        return None

    encoded_string = ''
    with Image.open(image_file) as img_f:
        img_f = img_f.resize(size=(width, height), resample=Image.ANTIALIAS)

        background = Image.new("RGB", (width, height), (255, 255, 255))
        background.paste(img_f, mask=img_f.split()[1])

        for xy in bodystamp_coordinates:
            x = int(float(xy[0]))
            y = int(float(xy[1]))
            r = 5

            draw = ImageDraw.Draw(background, 'RGBA')

            draw.ellipse(xy=(x - r, y - r, x + r, y + r), fill=(255,0,0, 150))
            del draw

        buffered = io.BytesIO()
        background.save(buffered, format=format)
        encoded_string = base64.b64encode(buffered.getvalue()).decode('utf-8)')

    return 'data:image/%s;base64,%s' % (format, encoded_string)
