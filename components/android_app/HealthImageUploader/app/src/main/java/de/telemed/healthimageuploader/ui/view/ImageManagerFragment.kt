package de.telemed.healthimageuploader.ui.view

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import de.telemed.healthimageuploader.R
import de.telemed.healthimageuploader.ui.viewmodel.CaseViewModel
import de.telemed.healthimageuploader.ui.viewmodel.ImageManagerViewModel
import de.telemed.healthimageuploader.ui.viewmodel.ImageManagerViewModelState
import kotlinx.android.synthetic.main.card_view_image.view.*
import kotlinx.android.synthetic.main.fragment_image_manager.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class ImageManagerFragment : Fragment() {
    private val TAG = "ImageManagerFragment"
    private val REQUEST_IMAGE_CAPTURE = 1
    private val model: ImageManagerViewModel by activityViewModels()
    private val caseModel: CaseViewModel by activityViewModels()
    private var currentImageUri: Uri? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        model.state().observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            modelStateHasChanged()
        })
        return inflater.inflate(R.layout.fragment_image_manager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        modelStateHasChanged()
        imageCardView.send_to_server_button.setOnClickListener {
            model.sendImage()
        }
        image_to_camera_button.setOnClickListener {
            takeAPicture()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            model.addImage(currentImageUri!!)
        }
    }

    private fun takeAPicture() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    getUniqueFilePath()
                } catch (ex: IOException) {
                    Log.e(TAG, "Wasn't possible to get an unique file path!")
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val imageUri = FileProvider.getUriForFile(
                        requireContext(),
                        "de.telemed.healthimageuploader.fileprovider",
                        it
                    )
                    currentImageUri = photoFile?.toUri()
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

    private fun getUniqueFilePath(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = context?.filesDir
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        )
    }

    private fun modelStateHasChanged() {
        Log.e(TAG, "Model state: ${model.state().value.toString()}")
        when(model.state().value) {
            ImageManagerViewModelState.NO_IMAGE -> {
                imageCardView.visibility = View.INVISIBLE
                sendProgressBar.visibility = View.INVISIBLE
                noImageTextView.visibility = View.VISIBLE
            }
            ImageManagerViewModelState.IMAGE_ADDED -> {
                noImageTextView.visibility = View.INVISIBLE
                sendProgressBar.visibility = View.INVISIBLE
                imageCardView.visibility = View.VISIBLE
                imageCardView.caseIdTextEdit.text = caseModel.isCaseIdAvailable()
                imageCardView.imageView.setImageBitmap(model.bitmap)
            }
            ImageManagerViewModelState.IMAGE_SEND -> {
                sendProgressBar.visibility = View.VISIBLE
            }
            ImageManagerViewModelState.IMAGE_SEND_ERROR -> {
                noImageTextView.visibility = View.INVISIBLE
                sendProgressBar.visibility = View.INVISIBLE
                imageCardView.visibility = View.VISIBLE
                imageCardView.caseIdTextEdit.text = caseModel.isCaseIdAvailable()
                imageCardView.imageView.setImageBitmap(model.bitmap)
                Toast.makeText(context, getString(R.string.image_fragment_toast_send_error), Toast.LENGTH_SHORT).show()
            }
            ImageManagerViewModelState.IMAGE_SENT -> {
                Toast.makeText(context, getString(R.string.image_fragment_toast_send_successful), Toast.LENGTH_SHORT).show()
                model.deleteCurrentImage()
                findNavController().navigate(R.id.action_imageManagerFragment_to_caseFragment)
            }
        }
    }
}