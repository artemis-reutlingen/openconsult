<!-- ABOUT THE PROJECT -->
## About The Project

**openConsult** is a collection of open-source components for implementation of a store-and-forward teledermatology service. This repository was modeled after and inspired by the publicly funded [TELEDerm Project](https://www.drks.de/drks_web/navigate.do?navigationId=trial.HTML&TRIAL_ID=DRKS00012944) in Germany. The goal of the openConsult project was to provide the necessary software components for small-scale research projects concerned with evaluation of this particular scenario. Usage of the [GDT-Interface](https://www.qms-standards.de/standards/gdt-schnittstelle/) allows for a partial integration (currently export of basic-patient data) with the patient data management systems in general medical practices. 

openConsult is currently designed only for the use case of teledermatology, different store-and-forward scenarios may be implemented with relatively low effort in the future. Due to a lack of funding, openConsult is currently not developed further. Feel free to contribute to the project or to contact one of the authors if you have any questions.

![openConsilt screenshot](documentation/screenshot_openconsult.png)

### Built With

* [Python 3.8](https://www.python.org/)
* [Django](https://www.djangoproject.com/)
* [Flask](https://flask.palletsprojects.com/en/2.0.x/)
* [Kotlin](https://kotlinlang.org/) (ImageUploader App by J.Schuh)


<!-- USAGE EXAMPLES -->
## Usage Scenario

The following diagram shows the general architecture of openConsult. The medical workflow is centered around the medical practice. The GP takes picture of dermatological lesions of patients and securely shares them with the dermatologist via the openConsult server/web-interface.

![openConsult architecture](/documentation/architecture/architecture.png)

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- CONTACT -->
## Contact
Oliver Bertram - oliver.bertram@reutlingen-university.de

Project Link: [https://gitlab.com/artemis-reutlingen/openconsult](https://gitlab.com/artemis-reutlingen/openconsult)


<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

Johannes Schuh - johannes.schuh@reutlingen-university.de (helpful advice for the overall architecture, implementation of the mobile app)




