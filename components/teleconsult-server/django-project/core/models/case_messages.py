from django.db import models


class CaseMessage(models.Model):
    case = models.ForeignKey('core.DermaConsultCase', on_delete=models.CASCADE)
    author = models.ForeignKey('users.User', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now=True)
    message = models.CharField(max_length=512, blank=False)

