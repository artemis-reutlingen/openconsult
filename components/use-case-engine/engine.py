from gdt_parser import parser
from use_case_engine import loggers
from gdt_json_mapper.mapper import GDTMapper
from gdt_json_mapper.constants import GDTFieldTokens as tokens
from use_case_engine.remote_calls import IdMappingService, RemoteService, PseudonymizationService, REMOTE_CONFIG
from use_case_engine.config import GdtMapperConfig
import webbrowser
from jsonschema import validate
from urllib3.exceptions import SSLError

LOGGER = loggers.LOGGER

gdt_mapper = GDTMapper()

key_selection_telederm = [
    tokens.GDT_3000,
    tokens.GDT_3102,
    tokens.GDT_3101,
    tokens.GDT_3103,
    tokens.GDT_3110
]


def main():

    LOGGER.info("Checking Service availability")
    if RemoteService.hello_server() != 200:
        LOGGER.critical("Remote service not reachable. Can't start Case Registration Routine.")
    else:
        LOGGER.info("Starting Case Registration Routine")
        file = "example_data/medistar_2.1_6302.gdt"
        gdt_parser = parser.GDTParser()
        parsed_gdt = gdt_parser.parse(file)

        mapped_json = gdt_mapper.map_gdt_dict_to_json(parsed_gdt, key_selection_telederm)
        validate(mapped_json, GdtMapperConfig.JSON_SCHEMA)

        new_case_id = RemoteService.post_case()

        IdMappingService.store_id_pair(mapped_json['patient_id'], new_case_id)
        IdMappingService.retrieve_id_pair(new_case_id)
        patient_id = PseudonymizationService.store_patient_info(mapped_json)
        pseudonymized_patient_info = PseudonymizationService.retrieve_patient_info_pseudonymized(patient_id)

        patient_dict = {
            'anamnesis_patient_age': pseudonymized_patient_info['age'],
            'anamnesis_patient_sex': pseudonymized_patient_info['gender']
        }

        case_url = RemoteService.put_case(new_case_id, patient_dict)

        webbrowser.open_new_tab(REMOTE_CONFIG.base_url + case_url)


if __name__ == "__main__":
    # execute only if run as a script
    main()
