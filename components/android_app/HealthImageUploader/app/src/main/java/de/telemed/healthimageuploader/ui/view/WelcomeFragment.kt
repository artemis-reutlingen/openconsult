package de.telemed.healthimageuploader.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import de.telemed.healthimageuploader.R
import de.telemed.healthimageuploader.model.AuthToken
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class WelcomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onStart() {
        super.onStart()
        val authToken = AuthToken()
        GlobalScope.launch {
            delay(1500)
            if(authToken.token() != null && authToken.isValid()) {
                findNavController().navigate(R.id.action_welcomeFragment_to_caseFragment)
            }
            else {
                findNavController().navigate(R.id.action_welcomeFragment_to_tokenFragment)
            }

        }
    }
}