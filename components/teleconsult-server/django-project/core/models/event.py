from django.db import models
from django.utils.translation import gettext_lazy as _


class Event(models.Model):
    class EventType(models.TextChoices):
        CREATED = 'CR', _('Case Created')
        PATIENT_DATA_UPDATED = 'PU', _('Patient data updated')
        ANAMNESIS_REVISED = 'AR', _('Anamnesis Revised')
        FINDING_REVISED = 'FR', _('Finding Revised')
        IMAGE_ADDED = 'IA', _('Image Added')
        IMAGE_DELETED = 'ID', _('Image Deleted')
        CASE_PUBLISHED = 'CP', _('Case Published')
        NEW_MESSAGE = 'NM', _('New Message')
        REVIEWER_ASSIGNED = 'RA', _('Reviewer Assigned')
        CASE_CLOSED = 'CC', _('Case Closed')

    description = models.CharField(
        max_length=2,
        choices=EventType.choices,
        default=EventType.CREATED,
    )
    case = models.ForeignKey('core.DermaConsultCase', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now=True)

