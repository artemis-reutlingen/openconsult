package de.telemed.healthimageuploader.ui.viewmodel

import androidx.lifecycle.ViewModel
import de.telemed.healthimageuploader.model.Case

class CaseViewModel: ViewModel() {
    private val case = Case()

    fun isCaseIdAvailable(): String? {
        return case.caseId()
    }

    fun addCaseId(caseId: String) {
        case.addCase(caseId)
    }

}