#!/bin/bash

if [ -d venv ]; then
  echo "Removing existing virtualenv."
  rm -rf venv
fi

virtualenv -p python3.8 venv

source venv/bin/activate

pip install --upgrade pip

cd ../gdt-parser && python setup.py install

cd ../gdt-json-mapper && python setup.py install

cd ../use-case-engine

pip install -r requirements.txt
