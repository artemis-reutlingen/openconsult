from app import api
from app.endpoints import MappingResourceList

# Register Resource endpiont classes to API object
api.add_resource(MappingResourceList, '/mappings')
