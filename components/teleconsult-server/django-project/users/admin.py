from django.contrib import admin
from users.models import User, Dermatologist, GeneralPracticioner


class UserAdmin(admin.ModelAdmin):
    model = User
    list_display = (
        'username',
        'id',
        'is_dermatologist',
        'is_general_practicioner',
        'first_name',
        'last_name',
        'assigned_region',

    )
    list_filter = (
        'assigned_region',
    )

admin.site.register(User, UserAdmin)


class DermatologistAdmin(admin.ModelAdmin):
    model = Dermatologist
    list_display = (
        'user',
        'is_currently_available'
    )
admin.site.register(Dermatologist, DermatologistAdmin)


class GeneralPracticionerAdmin(admin.ModelAdmin):
    model = GeneralPracticioner
    list_display = (
        'user',
        'pseudo_service_url',
    )

admin.site.register(GeneralPracticioner, GeneralPracticionerAdmin)
