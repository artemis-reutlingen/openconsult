"""empty message

Revision ID: 3bc786c9072a
Revises: 
Create Date: 2020-09-22 16:05:03.288249

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3bc786c9072a'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('patient',
    sa.Column('patient_id', sa.String(length=128), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('patient_birth_date', sa.Date(), nullable=True),
    sa.Column('patient_first_name', sa.String(length=128), nullable=True),
    sa.Column('patient_last_name', sa.String(length=128), nullable=True),
    sa.Column('patient_gender', sa.String(length=128), nullable=True),
    sa.PrimaryKeyConstraint('patient_id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('patient')
    # ### end Alembic commands ###
