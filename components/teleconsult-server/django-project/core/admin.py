from django.contrib import admin
from core.models import DermaConsultCase, RegionReviewer, Region, CaseQueueItem, Event, DermaImage, CaseMessage


class DermaConsultCaseAdmin(admin.ModelAdmin):
    model = DermaConsultCase

    list_display = (
        'id',
        'created_at',
        'requester',
        'examiner',
        'status',

    )
    list_filter = (
        'status',
    )
    date_hierarchy = 'created_at'

    search_fields = ('id', 'examiner__username', 'requester__username', 'status')

    ordering = ('-created_at',)

    list_per_page = 25


admin.site.register(DermaConsultCase, DermaConsultCaseAdmin)


class RegionAdmin(admin.ModelAdmin):
    model = Region
    list_display = (
        'code',
        'last_regionreviewer_id',
    )


admin.site.register(Region, RegionAdmin)


class RegionReviewerAdmin(admin.ModelAdmin):
    model = RegionReviewer
    list_display = (
        'id',
        'user',
        'region',
    )


admin.site.register(RegionReviewer, RegionReviewerAdmin)


class CaseQueueItemAdmin(admin.ModelAdmin):
    model = CaseQueueItem

    list_display = (
        'id',
        'case_id',
        'target_region',
        'requester',
        'examiner',
        'priority',
        'status',
    )
    list_filter = (
        'status',
        'target_region',
    )


admin.site.register(CaseQueueItem, CaseQueueItemAdmin)


class CaseMessageAdmin(admin.ModelAdmin):
    model = CaseMessage


admin.site.register(CaseMessage, CaseMessageAdmin)


class DermaImageAdmin(admin.ModelAdmin):
    model = DermaImage
    list_display = (
        'id',
        'case',
    )


admin.site.register(DermaImage, DermaImageAdmin)


class EventAdmin(admin.ModelAdmin):
    model = Event
    search_fields = ('case__id',)

    list_display = (
        'case',
        'description',
        'timestamp',
    )


admin.site.register(Event, EventAdmin)
