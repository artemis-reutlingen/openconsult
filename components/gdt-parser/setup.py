import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gdt_parser",
    version="0.0.1",
    author="Oliver Bertram",
    author_email="bertram_oliver@gmx.de",
    description="A GDT parser implementation.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bertram_o/telemed-framework/-/tree/develop/components/gdt-parser",
    packages=setuptools.find_packages(),
    install_requires=[
        'chardet==3.0.4',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)
