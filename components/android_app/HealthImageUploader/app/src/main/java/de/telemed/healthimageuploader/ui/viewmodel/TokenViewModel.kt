package de.telemed.healthimageuploader.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.telemed.healthimageuploader.R
import de.telemed.healthimageuploader.model.AppContext
import de.telemed.healthimageuploader.model.AuthToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat

class TokenViewModel: ViewModel() {
    private val authToken = AuthToken()
    private val isTokenValid = MutableLiveData(false)

    fun isTokenAvailable(): String? {
        return authToken.token()
    }

    fun tokenCreationTime(): String {
        val format = SimpleDateFormat(AppContext.appContext.getString(R.string.token_fragment_creation_time_format))
        return format.format(authToken.creationTime()?.timeInMillis)
    }

    fun isAuthTokenValid(): LiveData<Boolean> {
        viewModelScope.launch(Dispatchers.IO) {
            isTokenValid.postValue(authToken.isValid())
        }
        return isTokenValid
    }
}