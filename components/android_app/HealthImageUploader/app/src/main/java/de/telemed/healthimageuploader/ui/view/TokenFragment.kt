package de.telemed.healthimageuploader.ui.view

import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import de.telemed.healthimageuploader.R
import de.telemed.healthimageuploader.ui.viewmodel.ScannerMode
import de.telemed.healthimageuploader.ui.viewmodel.ScannerViewModel
import de.telemed.healthimageuploader.ui.viewmodel.TokenViewModel
import kotlinx.android.synthetic.main.card_view_token.*
import kotlinx.android.synthetic.main.card_view_token.view.*
import kotlinx.android.synthetic.main.fragment_token.*

class TokenFragment : Fragment() {
    private val model: TokenViewModel by activityViewModels()
    private val scannerModel: ScannerViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_token, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addTokenButton.setOnClickListener {
            scannerModel.mode = ScannerMode.AUTH_TOKEN
            findNavController().navigate(R.id.action_tokenFragment_to_scannerFragment)
        }
    }

    override fun onStart() {
        super.onStart()
        model.isAuthTokenValid().observe(this) {
            tokenStateChanged(it)
        }
        tokenStateChanged(model.isAuthTokenValid().value ?: false)
    }

    private fun tokenStateChanged(state: Boolean) {
        val token = model.isTokenAvailable()
        if(token != null) {
            tokenCardView.visibility = View.VISIBLE
            noTokenTextView.visibility = View.INVISIBLE
            tokenCardView.tokenIdTextView.text = token
            creationTimeTextView.text = model.tokenCreationTime()
        }
        else {
            tokenCardView.visibility = View.INVISIBLE
            noTokenTextView.visibility = View.VISIBLE
        }
        if(state) {
           tokenCardView.statusTextView.text = getString(R.string.token_fragment_valid)
            tokenCardView.statusTextView.setTextColor(Color.GREEN)
        }
        else {
            tokenCardView.statusTextView.text = getString(R.string.token_fragment_invalid)
            tokenCardView.statusTextView.setTextColor(Color.RED)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.case_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.action_menu_to_case_overview -> {
                findNavController().navigate(R.id.action_tokenFragment_to_caseFragment)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}