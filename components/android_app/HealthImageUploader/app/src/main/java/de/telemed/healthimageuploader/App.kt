package de.telemed.healthimageuploader

import android.app.Application
import de.telemed.healthimageuploader.model.AppContext

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        AppContext.appContext = applicationContext
    }
}