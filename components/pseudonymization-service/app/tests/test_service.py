import os
import unittest
import json
from app import app, db

TEST_DB = 'test.db'
BASE_DIR = app.root_path


class BasicTests(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(BASE_DIR, TEST_DB)
        self.app = app.test_client()
        db.drop_all()
        db.create_all()

        self.assertEqual(app.debug, False)

    def tearDown(self):
        db.drop_all()

    def test_post_patient(self):
        request_body = {
            "patient_id": "PVS_ID_00123",
            "patient_first_name": "Günther",
            "patient_last_name": "Meier",
            "patient_gender": "male",
            "patient_birth_date": "2012-04-23"
        }

        response = self.app.post('/api/v1/patients', json=request_body, follow_redirects=False)
        response_content = response.get_data(as_text=True).strip()[1:-1] # remove trailing newline, quotes -> ugly
        self.assertEqual(request_body['patient_id'], response_content)
        self.assertEqual(response.status_code, 201)
