# HealthImageUploader
This component is a simple Android app that a doctor uses to create medical images for selected patient cases and uploads them to the OpenConsult server.

## UseCase
![General usages](doc/BasicUsage.png)

## Screenshots

<center><table><tr><td>![WelcomeScreen](doc/WelcomeScreen.png)</td><td>![AuthTokenScreen](doc/AuthTokenScreen.png)</td><td>![CaseScreen](doc/CaseScreen.png)</td></tr><tr><td>![ImageScreen](doc/ImageScreen.png)</td><td>![ScannerScreen](doc/ScannerScreen.png)</td></tr></table>

## Built with
* Android
* Gradle
* [ML Kit](https://developers.google.com/ml-kit/vision/barcode-scanning/)
