from django.urls import path, include
from users.views import LandingPageView
from django.contrib import admin
urlpatterns = [
    path('', LandingPageView.as_view(), name='landing_page'),

    path('web/users/', include('users.urls')),
    path('web/cases/', include('core.urls')),
    path('api/', include('api.urls')),
    path('admin/', admin.site.urls),
    path('icd10/', include('icd10_catalog.urls')),

]
