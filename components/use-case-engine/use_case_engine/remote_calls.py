import requests

from use_case_engine.loggers import LOGGER
from use_case_engine.config import GlobalConstants, IdMappingServiceConfig, RemoteServiceConfig, \
    PseudonymizationServiceConfig
import toml
from jsonschema import validate

ID_MAPPER_CONFIG = IdMappingServiceConfig(**toml.load(GlobalConstants.ID_MAPPING_CFG_FILE))
REMOTE_CONFIG = RemoteServiceConfig(**toml.load(GlobalConstants.REMOTE_CFG_FILE))
PSEUDONYMIZATION_CONFIG = PseudonymizationServiceConfig(**toml.load(GlobalConstants.PSEUDONYMIZATION_CFG_FILE))


class IdMappingService:

    @classmethod
    def store_id_pair(cls, internal_id, external_id):
        url = ID_MAPPER_CONFIG.base_url + ID_MAPPER_CONFIG.Endpoints.STORE_ID_PAIR

        LOGGER.info(f"{cls.__name__} - calling store_id_pair server endpoint: {url}")
        r = requests.post(url=f'{url}',
                          json={
                              'internal_id': internal_id,
                              'external_id': external_id,
                              'use_case_id': GlobalConstants.USE_CASE_ID
                          }
                          )

        LOGGER.info(f"Response code: {r.status_code}")
        return r.status_code

    @classmethod
    def retrieve_id_pair(cls, external_id):
        url = ID_MAPPER_CONFIG.base_url + ID_MAPPER_CONFIG.Endpoints.STORE_ID_PAIR

        LOGGER.info(f"{cls.__name__} - calling retrieve_id_pair server endpoint: {url}")

        r = requests.get(
            url=f'{url}',
            params={
                'use_case_id': GlobalConstants.USE_CASE_ID,
                'external_id': external_id
            }
        )

        LOGGER.info(f"Response code: {r.status_code}")
        LOGGER.info(f"Response content: {r.json()}")

        return r.status_code


class RemoteService:
    @classmethod
    def obtain_auth_token(cls, url, username, password):
        r = requests.post(f'{url}', data={'username': username, 'password': password})

        if r.status_code == 200:
            json_dict = r.json()
            return json_dict['token']
        else:
            return None

    @classmethod
    def hello_server(cls):
        auth_token = REMOTE_CONFIG.auth_token
        url = REMOTE_CONFIG.base_url + REMOTE_CONFIG.Endpoints.HELLO_SERVER

        headers = {'Authorization': f"Token {auth_token}"}
        LOGGER.info(f"{cls.__name__} - calling hello server endpoint: {url}")

        r = requests.get(
            f'{url}',
            headers=headers,
            verify=REMOTE_CONFIG.server_pub_key_file,
            cert=(REMOTE_CONFIG.client_cert_file, REMOTE_CONFIG.client_key_file)
        )
        LOGGER.info(f"Response code: {r.status_code}")
        return r.status_code

    @classmethod
    def post_case(cls):
        auth_token = REMOTE_CONFIG.auth_token
        url = REMOTE_CONFIG.base_url + REMOTE_CONFIG.Endpoints.POST_CASE

        headers = {'Authorization': f"Token {auth_token}"}
        LOGGER.info(f"{cls.__name__} - calling post case server endpoint: {url}")

        r = requests.post(
            f'{url}',
            headers=headers,
            verify=REMOTE_CONFIG.server_pub_key_file,
            cert=(REMOTE_CONFIG.client_cert_file, REMOTE_CONFIG.client_key_file)
        )

        LOGGER.info(f"Response code: {r.status_code}")
        if r.status_code == 201:
            LOGGER.info(f"Response content {r.json()}")
            return r.json()['case_id']
        else:
            raise Exception("Could not post case")

    @classmethod
    def put_case(cls, case_id, patient_dict):
        validate(patient_dict, REMOTE_CONFIG.JSON_SCHEMA_UPDATE_PATIENT)
        auth_token = REMOTE_CONFIG.auth_token
        url = REMOTE_CONFIG.base_url + REMOTE_CONFIG.Endpoints.POST_CASE + case_id

        headers = {'Authorization': f"Token {auth_token}"}
        LOGGER.info(f"{cls.__name__} - calling put case server endpoint: {url}")

        r = requests.put(
            f'{url}',
            json=patient_dict,
            headers=headers,
            verify=REMOTE_CONFIG.server_pub_key_file,
            cert=(REMOTE_CONFIG.client_cert_file, REMOTE_CONFIG.client_key_file)
        )
        if r.status_code == 200:

            LOGGER.info(f"Response code: {r.status_code}")
            LOGGER.info(f"Response content {r.json()}")
            return r.json()['case_url']
        else:
            raise Exception("Could not update patient data")


class PseudonymizationService:
    @classmethod
    def store_patient_info(cls, patient_info):
        url = PSEUDONYMIZATION_CONFIG.base_url + PSEUDONYMIZATION_CONFIG.Endpoints.STORE_PATIENT_INFO
        LOGGER.info(f"{cls.__name__} - calling store_patient_info server endpoint: {url}")

        r = requests.post(f'{url}', json=patient_info)
        if r.status_code == 201:

            LOGGER.info(f"Response code: {r.status_code}")
            LOGGER.info(f"Response content {r.json()}")

            return r.json()

        else:
            raise Exception("Could not store patient data")

    @classmethod
    def retrieve_patient_info_pseudonymized(cls, patient_id):
        url = PSEUDONYMIZATION_CONFIG.base_url + PSEUDONYMIZATION_CONFIG.Endpoints.RETRIEVE_PATIENT_INFO
        LOGGER.info(f"{cls.__name__} - calling retrieve_patient_info_pseudonymized server endpoint: {url}")
        url = url + "/" + patient_id

        r = requests.get(
            url=f'{url}',
            params={
                'pseudonymized': 'true',
            }
        )

        LOGGER.info(f"Response code: {r.status_code}")
        LOGGER.info(f"Response content: {r.json()}")
        validate(r.json(), PSEUDONYMIZATION_CONFIG.JSON_SCHEMA_PSEUDONYMIZED)
        return r.json()
