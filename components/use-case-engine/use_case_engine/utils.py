from dataclasses import asdict

import toml

from use_case_engine.config import RemoteServices, GlobalConstants
from use_case_engine.loggers import LOGGER
from use_case_engine.remote_calls import obtain_auth_token


def obtain_token_dump_to_file(remote_config, username, password):
    token = obtain_auth_token(
        remote_config.base_url + RemoteServices.OBTAIN_AUTH_TOKEN,
        username,
        password
    )

    if token:
        LOGGER.info("got response with token, success")
        remote_config.auth_token = token
        config = {GlobalConstants.CFG_REMOTE_SECTION: asdict(remote_config)}
        config_file_descriptor = open(GlobalConstants.CFG_FILE, "w+")
        toml.dump(config, config_file_descriptor)
        return remote_config
    else:
        raise Exception("Got no auth token from server")
