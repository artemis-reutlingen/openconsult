from django.urls import path
from api import views
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path('v1/hello/', views.HelloView.as_view(), name='hello'),
    path('v1/cases/', views.Cases.as_view(), name='api-cases-post'),
    path('v1/cases/<uuid:case_id>', views.Cases.as_view(), name='api-cases-put'),
    path('v1/cases/<uuid:case_id>/images/', views.Images.as_view(), name='api-cases-put-image'),

    path('v1/auth-token/', obtain_auth_token, name='obtain-auth-token')
]
