from django.core.management.base import BaseCommand
from icd10_catalog.models import Icd10Item

import argparse
import csv

from telederm_service.settings import LOGGER


def check_positive_integer(value):
    integer_value = int(value)
    if integer_value <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return integer_value


class Command(BaseCommand):
    help = 'Runs initializiation script for ICD10 database table. Takes path to ICD10 csv file as input.'

    def add_arguments(self, parser):
        parser.add_argument('catalog_file', type=argparse.FileType('r'))
        parser.add_argument('no_of_entries', nargs='?', type=check_positive_integer)

    def handle(self, *args, **options):
        LOGGER.info("*** Initialize Script for ICD10 catalog table ***")
        LOGGER.info(f"Input file: {options['catalog_file'].name}")
        if options['no_of_entries'] is not None:
            LOGGER.info(f"Number of entries to read was specified: {options['no_of_entries']}")

        LOGGER.info("Clearing ICD10 catalog table first ...")
        Icd10Item.objects.all().delete()

        with options['catalog_file'] as input_file:
            counter = 0
            spamreader = csv.reader(input_file, delimiter='|')
            for row in spamreader:
                if options['no_of_entries'] is not None:
                    if counter == options['no_of_entries']:
                        LOGGER.info(f"\nStopped after inserting the specified number of entries.")
                        break
                if counter != 0:

                    if counter % 1000 == 0:
                        LOGGER.info(f"Work in progress - currently at {counter} catalog items.")

                icd_10_code = row[3]
                icd_10_description = row[7]

                counter += 1
                if icd_10_code == "":
                    # empty code section not suitable here
                    pass
                else:
                    catalog_item = Icd10Item(code=icd_10_code, description=icd_10_description)
                    catalog_item.save()
            LOGGER.info(f"{counter} ICD10 items have been inserted to the database.")


# Info about CSV structure of DIMDI file
"""
    Feld 1: Art der Kodierung
            0 = reiner Verweissatz
                Diese Datensätze haben keine Schlüsselnummer,
                sondern innerhalb des Textfeldes Verweise auf andere Begriffe,
                unter denen ihre Schlüsselnummern nachgewiesen sind.
            1 = Kodierung nur mit einer Primärschlüsselnummer
            2 = Kodierung mit einer Kreuz- und einer Sternschlüsselnummer
            3 = Kodierung mit einer Primärschlüsselnummer und mit
                einer Zusatzschlüsselnummer mit Ausrufezeichen
            4 = Kodierung mit einer Kreuz-, einer Stern- und einer
                Zusatzschlüsselnummer
            5 = Kodierung nur als Zusatzschlüsselnummer möglich
            6 = Kodierung mit zwei Primärschlüsselnummern
    Feld 2: DIMDI-interne fortlaufende Nummer
    Feld 3: Druckkennzeichen
            0 = Satz erscheint nicht in der Buchversion
            1 = Satz erscheint in der Buchversion
    Feld 4: Primärschlüsselnummer 1 (ggf. mit Kreuz)
    Feld 5: Sternschlüsselnummer (mit Stern)
    Feld 6: Zusatzschlüsselnummer (mit Ausrufezeichen)
    Feld 7: Primärschlüsselnummer 2 (ggf. mit Kreuz)
    Feld 8: zugehöriger Text, ggf. mit Verweis
"""
