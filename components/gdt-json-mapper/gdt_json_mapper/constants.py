class GDTFieldIDs():
    GDT_0102 = '0102'
    GDT_0103 = '0103'
    GDT_0132 = '0132'
    GDT_0201 = '0201'
    GDT_0203 = '0203'
    GDT_0211 = '0211'
    GDT_0212 = '0212'
    GDT_3000 = '3000'
    GDT_3100 = '3100'
    GDT_3101 = '3101'
    GDT_3102 = '3102'
    GDT_3103 = '3103'
    GDT_3104 = '3104'
    GDT_3105 = '3105'
    GDT_3106 = '3106'
    GDT_3107 = '3107'
    GDT_3108 = '3108'
    GDT_3110 = '3110'
    GDT_3622 = '3622'
    GDT_3623 = '3623'
    GDT_3628 = '3628'
    GDT_4104 = '4104'
    GDT_4111 = '4111'
    GDT_4121 = '4121'
    GDT_8000 = '8000'
    GDT_8100 = '8100'
    GDT_8315 = '8315'
    GDT_8316 = '8316'
    GDT_8402 = '8402'
    GDT_8410 = '8410'
    GDT_9206 = '9206'
    GDT_9218 = '9218'

class GDTFieldTokens():
    GDT_0102 = 'software_provider'
    GDT_0103 = 'software_name'
    GDT_0132 = 'software_version'
    GDT_0201 = 'bsnr'
    GDT_0203 = 'name_bsnr'
    GDT_0211 = 'doctor_name'
    GDT_0212 = 'lanr'
    GDT_3000 = 'patient_id'
    GDT_3100 = 'patient_prefix'
    GDT_3101 = 'patient_last_name'
    GDT_3102 = 'patient_first_name'
    GDT_3103 = 'patient_birth_date'
    GDT_3104 = 'patient_title'
    GDT_3105 = 'patient_insurance_number'
    GDT_3106 = 'patient_address_zip_city'
    GDT_3107 = 'patient_address_street'
    GDT_3108 = 'patient_insurance_type'
    GDT_3110 = 'patient_gender'
    GDT_3622 = 'patient_height'
    GDT_3623 = 'patient_weight'
    GDT_3628 = 'patient_language'
    GDT_4104 = 'vertragskassen_nr'
    GDT_4111 = 'kostentraeger'
    GDT_4121 = 'gebuehrenordnung_kvdt'
    GDT_8000 = 'sentence_id'
    GDT_8100 = 'sentence_length'
    GDT_8315 = 'gdt_receiver_id'
    GDT_8316 = 'gdt_sender_id'
    GDT_8402 = 'device_process_id'
    GDT_8410 = 'test_id'
    GDT_9206 = 'type_set'
    GDT_9218 = 'gdt_version'
