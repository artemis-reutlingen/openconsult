from django.shortcuts import render
from dal import autocomplete
from icd10_catalog.models import Icd10Item


class Icd10AutoComplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !

        qs = Icd10Item.objects.all()

        if self.q:
            # qs = qs.filter(name__istartswith=self.q)
            qs = qs.filter(code__icontains=self.q) | qs.filter(description__icontains=self.q)
        return qs
