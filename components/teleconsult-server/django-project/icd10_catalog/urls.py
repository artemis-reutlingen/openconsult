from django.urls import path
from icd10_catalog import views

urlpatterns = [
    path('lookup/', views.Icd10AutoComplete.as_view(), name='icd-10-lookup'),
]
