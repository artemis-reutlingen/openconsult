from gdt_json_mapper.handlers import GDTHandler
from gdt_json_mapper.loggers import LOGGER
from datetime import datetime
from gdt_json_mapper.constants import GDTFieldTokens


class GDTValueHandler:
    """
    Quick n dirty.
    TODO: allow users to "register" value handlers separately
    """

    def handle(self, request: dict) -> dict:
        LOGGER.debug(f"{self.__class__.__name__} Transforming GDT values (birthdate, gender)")
        birth_date_key = GDTFieldTokens.GDT_3103
        if birth_date_key in request.keys():
            raw_date_value = request[birth_date_key]
            LOGGER.debug(f"input date value: {raw_date_value}")
            datetimeobject = datetime.strptime(raw_date_value, '%d%m%Y')
            new_date_value = datetimeobject.strftime('%Y-%m-%d')
            LOGGER.debug(f"transformed date string: {new_date_value}")
            request[birth_date_key] = new_date_value

        gender_key = GDTFieldTokens.GDT_3110
        if gender_key in request.keys():
            raw_gender_value = request[gender_key]
            LOGGER.debug(f"input gender value: {raw_gender_value}")
            if raw_gender_value == '1':
                request[gender_key] = "M"
            elif raw_gender_value == '2':
                request[gender_key] = "F"
            else:
                request[gender_key] = "diverse"
            LOGGER.debug(f"transformed gender string: {request[gender_key]}")
        return request


class GDTMapper:
    message_handler = GDTHandler()

    def map_gdt_dict_to_json(self, gdt_input: dict, relevant_keys=None) -> dict:
        LOGGER.debug(f"Received mapping request for gdt dictionary: {gdt_input}")

        self.remove_gdt_9901_entry(gdt_input)

        mapper = self.message_handler.handle_gdt_dict(gdt_input)

        LOGGER.debug("Mapping GDT dictionary to TeleMed-Framework keyword definitions.")
        mapped_dict = self.generate_mapped_dictionary(mapper=mapper, gdt_dict=gdt_input)
        LOGGER.debug(f"Mapped dict result: {mapped_dict}")

        if relevant_keys is not None:
            LOGGER.debug(f"Extracting relevant keys: {relevant_keys}")
            mapped_dict = self.extract_relevant_keys(relevant_keys, mapped_dict)
            LOGGER.debug(f"Mapped dict after extraction of relevant keys: {mapped_dict}")

        value_handler = GDTValueHandler()
        transformed_dict = value_handler.handle(mapped_dict)
        LOGGER.debug(f"Resulting dictionary after transforming GDT values: {transformed_dict}")

        return transformed_dict

    def generate_mapped_dictionary(self, mapper, gdt_dict):
        mapped_dict = {}
        for key in gdt_dict:
            value = gdt_dict[key]
            new_key = mapper[key]['field_name']
            mapped_dict[new_key] = value
        return mapped_dict

    def extract_relevant_keys(self, relevant_keys, mapped_dict):
        relevant_data_dict = {}
        for key in relevant_keys:
            relevant_data_dict[key] = mapped_dict[key]
        return relevant_data_dict

    def remove_gdt_9901_entry(self, gdt_dict: dict) -> None:
        """
        Das Feld 9901 "Systeminterner Parameter, "a", "var" definiert, ist in keiner Satztabelle enthalten;
        es darf an beliebigen Stellen im Datenstrom auftauchen und kann vom Importeur ignoriert (überlesen) werden.
        """
        LOGGER.debug("Removing entry for GDT field id 9901 (can be ignored when processing GDT messages)")
        gdt_dict.pop("9901", None)
