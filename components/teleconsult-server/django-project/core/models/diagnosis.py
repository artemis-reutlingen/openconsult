from django.db import models


class CaseDiagnosis(models.Model):
    case = models.ForeignKey('core.DermaConsultCase', on_delete=models.SET_NULL, null=True)
    diagnosis = models.ForeignKey('icd10_catalog.Icd10Item', on_delete=models.SET_NULL, null=True)

    class Meta:
        abstract = True


class ExistingDiagnosis(CaseDiagnosis):
    pass


class ReviewDiagnosis(CaseDiagnosis):
    class Meta:
        permissions = (
            ('delete_review_diagnosis', 'Delete Review Diagnosis'),
        )
