from rest_framework import serializers
from core.models import DermaConsultCase


class PatientDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = DermaConsultCase
        fields = ['anamnesis_patient_sex', 'anamnesis_patient_age']
