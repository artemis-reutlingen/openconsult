from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _


class ServerAnnouncement(models.Model):
    class YesNoChoice(models.TextChoices):
        YES = 'yes', _('Yes')
        NO = 'no', _('No')

    created_at = models.DateTimeField(default=now, editable=False)

    subject = models.TextField(blank=True)
    message = models.TextField(blank=True)

    is_active = models.CharField(
        max_length=64,
        choices=YesNoChoice.choices,
        default=YesNoChoice.NO,
        blank=False
    )
