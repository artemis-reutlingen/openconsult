from core import views
from django.urls import path

urlpatterns = [
    path('create/', views.CreateCase.as_view(), name='create-case'),
    path('<uuid:pk>/', views.CaseDetail.as_view(), name='case-detail'),
    path('<uuid:pk>/report', views.PDFReport.as_view(), name='generate-pdf'),

    path('<uuid:pk>/publish', views.CreateCaseQueueItem.as_view(), name='publish-case'),
    path('<uuid:pk>/close', views.CloseCase.as_view(), name='finish-case'),
    path('<uuid:pk>/remove', views.DeleteCase.as_view(), name='remove-case'),
    path('<uuid:pk>/anamnesis/edit/', views.UpdateAnamnesis.as_view(), name='edit-anamnesis'),
    path('<uuid:pk>/finding/edit/', views.EditFinding.as_view(), name='edit-finding'),
    path('<uuid:pk>/bodystamp/edit/', views.EditBodystamp.as_view(), name='edit-bodystamp'),

    path('<uuid:pk>/images', views.AddImage.as_view(), name='add-image'),
    path('<uuid:pk>/messages', views.CreateMessage.as_view(), name='add-message'),
    path('<uuid:pk>/finding/diagnoses', views.CreateReviewDiagnosis.as_view(), name='add-finding-diagnosis'),
    path('reviewdiagnoses/<int:pk>', views.DeleteReviewDiagnosis.as_view(), name='delete-finding-diagnosis'),

    path('images/<int:pk>/delete', views.DeleteImage.as_view(), name='delete-image'),
    path('images/<int:pk>/edit', views.EditImage.as_view(), name='edit-image'),

    path('dashboard/', views.Dashboard.as_view(), name='dashboard'),
    path('', views.CaseList.as_view(), name='case-list'),

]
