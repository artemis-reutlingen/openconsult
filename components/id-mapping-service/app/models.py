from app import db
from datetime import datetime


class Mapping(db.Model):

    id = db.Column(db.Integer, primary_key=True)

    external_id = db.Column(db.String(100))
    internal_id = db.Column(db.String(100))
    use_case_id = db.Column(db.String(100))
    created_at = db.Column(db.DateTime, default=datetime.now)

    def __init__(self, external_id, internal_id, use_case_id):
        self.external_id = external_id
        self.internal_id = internal_id
        self.use_case_id = use_case_id

