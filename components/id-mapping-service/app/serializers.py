from app import ma
from app.models import Mapping


class MappingSchemaOut(ma.SQLAlchemyAutoSchema):
    # separate class for serializing
    class Meta:
        model = Mapping
        fields = ("external_id", "internal_id", "use_case_id", "creation_time")


class MappingSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Mapping
        load_instance = True
        fields = ("external_id", "internal_id", "use_case_id")


mapping_schema = MappingSchema()
mappings_schema = MappingSchema(many=True)
mapping_schema_out = MappingSchemaOut()
mappings_schema_out = MappingSchemaOut(many=True)
