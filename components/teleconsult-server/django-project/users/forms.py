from django.contrib.auth.forms import UserCreationForm
from django.db import transaction

from users.models import Dermatologist, GeneralPracticioner, User

USER_SIGNUP_FIELDS_GENERAL = (
    "username",
    "medical_title",
    "first_name",
    "last_name",
    "lanr",
    "phone_number",
    "email",
    "password1",
    "password2",
    "address_streetname",
    "address_house_number",
    "address_zip_code",
    "address_city",
    "assigned_region"
)


class DermatologistSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = USER_SIGNUP_FIELDS_GENERAL

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_dermatologist = True
        user.save()
        dermatologist = Dermatologist.objects.create(user=user)
        return user


class GeneralPracticionerSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = USER_SIGNUP_FIELDS_GENERAL

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_general_practicioner = True
        user.save()
        general_practicioner = GeneralPracticioner.objects.create(user=user)
        return user
