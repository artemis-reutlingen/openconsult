package de.telemed.healthimageuploader.model

import android.content.Context
import java.util.*

class AuthToken {
    private val TOKEN = "auth_token"
    private val CREATION_DATETIME = "auth_token_creation_datetime"

    fun token(): String? {
        return readToken()
    }

    fun creationTime(): Calendar? {
        return readCreationTime()
    }

    fun update(newToken: String) {
        writeInFile(newToken, Calendar.getInstance())
    }

    private fun readToken(): String? {
        val sharedPerf = AppContext.appContext.getSharedPreferences("preference_file", Context.MODE_PRIVATE)
        return sharedPerf.getString(TOKEN, null)

    }

    private fun readCreationTime(): Calendar? {
        val sharedPerf = AppContext.appContext.getSharedPreferences("preference_file", Context.MODE_PRIVATE)
        val longValue =  sharedPerf.getLong(CREATION_DATETIME, 0L)
        if(longValue == 0L) {
            return null
        }
        val cal = Calendar.getInstance()
        cal.timeInMillis = longValue
        return cal
    }

    private fun writeInFile(token: String, creationDate: Calendar) {
        val sharedPerf = AppContext.appContext.getSharedPreferences("preference_file", Context.MODE_PRIVATE) ?: return
        with(sharedPerf.edit()) {
            putString(TOKEN, token)
            putLong(CREATION_DATETIME, creationDate.timeInMillis)
            apply()
        }
    }

    suspend fun isValid(): Boolean {
        val token = readToken()
        if(token != null) {
            val response = ServerAccess.isTokenValid(token)
            return response.first
        }
        return false
    }


}