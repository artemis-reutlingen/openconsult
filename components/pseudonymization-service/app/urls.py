from app import api
from app.endpoints import PatientResourceList, PatientResource

# Register Resource endpiont classes to API object
api.add_resource(PatientResourceList, '/patients')
api.add_resource(PatientResource, '/patients/<patient_id>')
