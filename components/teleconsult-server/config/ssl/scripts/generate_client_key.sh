#!/bin/bash

CLIENT_ID="hausarzt"
CLIENT_SERIAL=01
PASSWORD="GEHEIM"

openssl genrsa -aes256 -passout pass:${PASSWORD} -out client.${CLIENT_ID}.pass.key 4096
openssl rsa -passin pass:${PASSWORD} -in client.${CLIENT_ID}.pass.key -out client.${CLIENT_ID}.key
rm client.${CLIENT_ID}.pass.key

openssl req -new -key client.${CLIENT_ID}.key -out client.${CLIENT_ID}.csr

openssl x509 -req -days 365 -in client.${CLIENT_ID}.csr -CA server.cert.pem -CAkey server.key.pem -set_serial ${CLIENT_SERIAL} -out client.${CLIENT_ID}.pem

# Bundle files
# cat ${CLIENT_ID}.key ${CLIENT_ID}.pem cert.pem > ${CLIENT_ID}.full.pem
openssl pkcs12 -export -out client.${CLIENT_ID}.full.pfx -inkey client.${CLIENT_ID}.key -in client.${CLIENT_ID}.pem -certfile server.cert.pem
