from django.apps import AppConfig


class Icd10CatalogConfig(AppConfig):
    name = 'icd10_catalog'
