import unittest
from gdt_parser.parser import GDTParser


class ParserTest(unittest.TestCase):
    parser = GDTParser()

    def test_with_examples(self):
        filenames = [
            'medistar_2.1_6302.gdt',
            'some_pms_2.0_6302.gdt',
            'xconcept_2.0_6301.gdt'
        ]

        file_path_prefix = "gdt_parser/test/gdt_examples/"

        for filename in filenames:
            try:
                self.parser.parse(file_path_prefix + filename)
            except Exception:
                self.fail("Parsing GDT examples raised Exception unexpectedly!")
