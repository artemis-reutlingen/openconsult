from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from users import views

urlpatterns = [
    path('signup/', views.SignUpView.as_view(), name='signup'),
    path('signup/dermatologist/', views.DermatologistSignUpView.as_view(), name='dermatologist_signup'),
    path('signup/general_practicioner/', views.GeneralPracticionerSignUpView.as_view(),
         name='general_practicioner_signup'),
    path('login/', LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', LogoutView.as_view(next_page='landing_page'), name='logout'),
    path('profile/', views.MemberPageView.as_view(), name='member_page'),
    path('profile/update/<int:pk>/', views.UserUpdateView.as_view(), name='user_update'),
    path('profile/update/password/', views.change_password, name='change-password'),
    path('profile/update/<int:pk>/email/', views.UserEmailUpdateView.as_view(), name='change-email'),
    path('profile/derma/<int:pk>/', views.DermatologistAvailabilityView.as_view(), name='set_availability'),
    path('profile/gp/<int:pk>/', views.GeneralPracticionerPseudoURLView.as_view(), name='set_pseudo_url'),
    path('profile/update/<int:pk>/authtoken/', views.TokenUpdate.as_view(), name='auth-token'),

]
