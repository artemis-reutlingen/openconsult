import json

from PIL import Image
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import reverse
from guardian.shortcuts import assign_perm

from rest_framework.exceptions import PermissionDenied, ParseError
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers import PatientDataSerializer
from core.models import DermaConsultCase, DermaImage
from telederm_service.settings import LOGGER


class HelloView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        content = {'message': 'Hello, World!'}
        return Response(content)


class Cases(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        # TODO: a more generic solution would be preferable
        if not self.request.user.has_perm('core.add_dermaconsultcase'):
            raise PermissionDenied

        LOGGER.debug(f"POST CASE API ENDPOINT")
        LOGGER.debug(f"user: {request.user.username}")

        # TODO: DUPLICATED CODE FROM CORE/VIEWS -> Create Case logic
        case_object = DermaConsultCase.objects.create(requester=self.request.user)

        assign_perm('core.access_dermaconsultcase', self.request.user, case_object)
        assign_perm('core.edit_dermaconsultcase_anamnesis', self.request.user, case_object)
        assign_perm('core.publish_dermaconsultcase', self.request.user, case_object)
        assign_perm('core.remove_dermaconsultcase', self.request.user, case_object)

        LOGGER.debug(f"Created new case resource with id {str(case_object.id)}")
        response_dict = {'case_id': str(case_object.id)}  # convert UUID type to string
        response_string = json.dumps(response_dict)

        return HttpResponse(status=201, content=response_string)

    def put(self, request, case_id):

        case_object = get_object_or_404(DermaConsultCase, pk=case_id)
        # TODO: a more generic solution would be preferable
        if not self.request.user.has_perm('core.edit_dermaconsultcase_anamnesis', case_object):
            raise PermissionDenied

        LOGGER.debug(f"PUT CASE API ENDPOINT")
        LOGGER.debug(f"user: {request.user.username}")
        LOGGER.debug(f"case id: {case_id}")

        request_body_dict = json.loads(request.body)
        LOGGER.debug(request_body_dict)

        serializer = PatientDataSerializer(data=request_body_dict)

        if serializer.is_valid(raise_exception=True):
            LOGGER.debug(f"serializer.is_valid() = {serializer.is_valid()}")

            patient_age = serializer.validated_data["anamnesis_patient_age"]
            patient_gender = serializer.validated_data["anamnesis_patient_sex"]

            case_object.update_patient_data(patient_age=patient_age, patient_sex=patient_gender)

            case_object.save()

            response_dict = {'case_url': reverse("case-detail", kwargs={"pk": case_id})}

            response_string = json.dumps(response_dict)

            return HttpResponse(status=200, content=response_string)


class ImageUploadParser(FileUploadParser):
    media_type = 'image/*'


class Images(APIView):
    permission_classes = (IsAuthenticated,)
    parser_class = (ImageUploadParser,)

    def post(self, request, case_id, format=None):

        case_object = get_object_or_404(DermaConsultCase, pk=case_id)

        if not self.request.user.has_perm('core.edit_dermaconsultcase_anamnesis', case_object):
            raise PermissionDenied

        if 'file' not in request.data:
            raise ParseError("Request data did not contain 'file' key")

        f = request.data['file']

        # verify that uploaded file is image
        try:
            img = Image.open(f)
            img.verify()
        except:
            raise ParseError("Unsupported image type")

        new_image = DermaImage.objects.create(case=case_object)

        new_image.image = f
        new_image.save()

        user = self.request.user
        assign_perm('core.delete_image', user, new_image)

        return HttpResponse(status=201)
