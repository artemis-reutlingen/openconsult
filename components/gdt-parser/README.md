# GDT-Parser

This component is responsible for:
- Take *.gdt file as input
- Confirm GDT compliance
- Parse information from GDT file
- Return a Python dictionary OR JSON file containing all original information