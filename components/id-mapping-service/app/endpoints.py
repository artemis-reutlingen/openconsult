from flask import request, send_from_directory
from flask_restful import Resource
from marshmallow import ValidationError
from app import app, db
from app.models import Mapping
from app.serializers import mapping_schema, mappings_schema_out


class MappingResourceList(Resource):
    def get(self):
        use_case_id = request.args.get('use_case_id')
        external_id = request.args.get('external_id')

        query = Mapping.query

        if use_case_id:
            query = query.filter(Mapping.use_case_id == use_case_id)

        if external_id:
            query = query.filter(Mapping.external_id == external_id)

        return mappings_schema_out.dump(query), 200

    def post(self):
        json_dict = request.get_json(force=True)

        try:
            new_item = mapping_schema.load(json_dict, session=db.session)  # Session??
            db.session.add(new_item)
            db.session.commit()
            return new_item.id, 201
        except ValidationError as err:
            print(err.messages)
            return err.messages, 400


# TODO: necessary endpoint???
@app.route('/static/<path>')
def send_static(path):
    return send_from_directory('static', path)
